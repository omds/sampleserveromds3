package at.vvo.omds3.exampleservice;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.apache.oltu.oauth2.as.request.OAuthAuthzRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;

public class OAuthServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	        throws IOException {
	 
	    try {
	         OAuthAuthzRequest oauthRequest = new OAuthAuthzRequest(request);
	         //build OAuth response
	         OAuthResponse resp = OAuthASResponse
	             .authorizationResponse(request,HttpServletResponse.SC_FOUND)
	             .setCode("")
	             .location("")
	             .buildQueryMessage();
	 
	         response.sendRedirect(resp.getLocationUri());
	 

	    } catch(OAuthProblemException ex) {
	         OAuthResponse resp;
			try {
				resp = OAuthASResponse
				     .errorResponse(HttpServletResponse.SC_FOUND)
				     .error(ex)
				     .location("")
				     .buildQueryMessage();
			} catch (OAuthSystemException e) {
				e.printStackTrace();
			}
	 
	         response.sendRedirect("");
	    } catch(OAuthSystemException ex) {
	         OAuthResponse resp;
			try {
				resp = OAuthASResponse
				         .errorResponse(HttpServletResponse.SC_FOUND)
				         .location("")
				         .buildQueryMessage();
			} catch (OAuthSystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		         response.sendRedirect("");
		    }
	 
	}
	
}
