<%@ page import="at.vvo.omds3.exampleservice.Version" %>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <!--                                                               -->
    <!-- Consider inlining CSS to reduce the number of requested files -->
    <!--                                                              
    <link type="text/css" rel="stylesheet" href="AllverBackend.css"> -->

    <!--                                           -->
    <!-- Any title is fine                         -->
    <!--                                           -->
    <title>OMDS Example-Server</title>
    
    <!--                                           -->
    <!-- This script loads your compiled module.   -->
    <!-- If you add any GWT meta tags, they must   -->
    <!-- be added before this line.                -->
    <!--                                           -->
    <script type="text/javascript" src="omdsexampleapp/omdsexampleapp.nocache.js"></script>
    <link rel="icon" href="/img/favicon.png" type="image/png">
  </head>
  <body>
  	<img src="img/VVO_Logo.png" />
    <h1>OMDS Example-Server</h1>
    <p>Diese Applikation agiert wie ein VU-Server und liefert "Fake"-Daten.</p>

    <p><a href="services/" title="Liste der Services">Liste der Services</a></p>
    
    <p><%=Version.getVersion() %>
  </body>
</html>
