package at.vvo.omds3.exampleservice.fake.jobs;

import at.vvo.omds3.exampleservice.fake.VuModel;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Ein Quarz-Job, der Polizzen nacherzeugt und neue OMDS Pakete
 * @author Jens Buehring
 *
 */
public class CreateAddPoliciesJob implements org.quartz.Job {
	
	public CreateAddPoliciesJob() {
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {
		VuModel.getInstance().createAdditionalPolicies();
	}
}	

