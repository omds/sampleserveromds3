package at.vvo.omds3.exampleservice.fake.polDocuments;

import java.io.Serializable;

public class MCArcImageRequest implements Serializable {
	
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String vuNr;
//    protected AuthorizationFilter authFilter;
    protected String arcImageId;
    
	public String getVuNr() {
		return vuNr;
	}
	public void setVuNr(String vuNr) {
		this.vuNr = vuNr;
	}
	public String getArcImageId() {
		return arcImageId;
	}
	public void setArcImageId(String arcImageId) {
		this.arcImageId = arcImageId;
	}
    
    


}
