package at.vvo.omds3.exampleservice.fake.polDocuments;

import at.vvo.omds3.exampleservice.FileCodecBase64;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class ErzeugerPolizzendokumente {
	
	private static final DecimalFormat VUNR_FORMAT = new DecimalFormat("0000");

	public static final String MIMETYPE_PDF = "application/pdf";
	
	public static List<MCArcContent> fakePoliciesListe() {
		
		ArrayList<MCArcContent> liste = new ArrayList<>();
		
		for (int i = 0; i < 3; i++) {
			
			try {
				MCArcContent content = new MCArcContent();

				MCArcImageInfo model = new MCArcImageInfo();
				model.setArcContentType(MIMETYPE_PDF);
				model.setName("Policy Document "+String.valueOf(i));
				model.setArcContentLength((long) 1256456);
				model.setDate(LocalDateTime.of(2019, 05,
						22, 0, 0, 0, 0));
				model.setArcImageIdDetails(typeModel(i));

				content.setArcImageInfo(model);
				content.setArcImage(setXmlFiles(i));
				liste.add(content);
				
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return liste;
	}
	
	private static MCElementIdTypeModel typeModel(int i) {
		
		MCElementIdTypeModel idType = new MCElementIdTypeModel();
		idType.setId(VUNR_FORMAT.format(i));
		idType.setIdIsSingleUse(false);
		idType.setIdValidUntil(null);
		
		return idType;
	}
	
	private static byte[] setXmlFiles(int i) throws IOException  {
		InputStream in = ErzeugerPolizzendokumente.class.getClassLoader().getResourceAsStream("policy_"+String.valueOf(i)+".pdf");
		byte[] arr = FileCodecBase64.getInputStreamAsByteArray(in);
		return arr;
	}

}
