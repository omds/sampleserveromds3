package at.vvo.omds3.exampleservice.fake.polDocuments;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class MCArcImageInfosRequest implements Serializable{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected String vuNr;
//    protected AuthorizationFilter authFilter;
    protected String policyNumber;
    protected String policyType;
    protected List<MCPolicyPartnerRole> policyPartyRole;
    protected List<Integer> documentType;
    protected Date dateFrom;
    protected Date dateUntil;
    
	public String getVuNr() {
		return vuNr;
	}
	public void setVuNr(String vuNr) {
		this.vuNr = vuNr;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public List<MCPolicyPartnerRole> getPolicyPartyRole() {
		return policyPartyRole;
	}
	public void setPolicyPartyRole(List<MCPolicyPartnerRole> policyPartyRole) {
		this.policyPartyRole = policyPartyRole;
	}
	public List<Integer> getDocumentType() {
		return documentType;
	}
	public void setDocumentType(List<Integer> documentType) {
		this.documentType = documentType;
	}
	public Date getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Date getDateUntil() {
		return dateUntil;
	}
	public void setDateUntil(Date dateUntil) {
		this.dateUntil = dateUntil;
	}
    
    

}
