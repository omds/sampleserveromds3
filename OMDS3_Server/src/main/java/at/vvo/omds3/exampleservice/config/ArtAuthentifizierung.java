package at.vvo.omds3.exampleservice.config;

/**
 * Die Art der Authentifizierung gegenueber der Versicherung.
 * 
 * @author Jens Buehring
 *
 */
public enum ArtAuthentifizierung {
	
	USERNAME_TOKEN,
	SECURITY_CONTEXT_TOKEN;

}
