package at.vvo.omds3.exampleservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Properties;

/**
 * @author Jens Buehring
 *
 */
public final class Version {
	
	private static final String PROPFILE = "version.properties";
	
	public static final String APPLICATION;
	public static final String VERSION;
	public static LocalDateTime releaseDate;
	
	private static final Logger LOG = LoggerFactory.getLogger(Version.class);

	/** Constructor welcher den default Constructor versteckt */
	private Version() {}

	static {
		Properties versionProps = new Properties();
		try {
			//ClassLoader classLoader = this.getClass().getClassLoader();
			//InputStream in = classLoader.getResourceAsStream(path);
			
			InputStream is = Version.class.getClassLoader().getResourceAsStream(PROPFILE);
			versionProps.load(is);
		} catch (IOException e) {
			LOG.atWarn().log("IO Exception beim Lesen des " + PROPFILE + " File"	+ e.getMessage());
		}

		VERSION = versionProps.getProperty("version");
		APPLICATION = versionProps.getProperty("application");
		releaseDate = null;
		
		try {
			String releaseDateProperty = versionProps.getProperty("releasedate");
			LOG.atDebug().log("releasedate=" + releaseDateProperty);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
			releaseDate = LocalDateTime.parse(releaseDateProperty, formatter);
			//releaseDate = new LocalDateTime(new SimpleDateFormat("yyyyMMdd").parse(versionProps.getProperty("releasedate")));
			//releaseDate.setTime(new SimpleDateFormat("yyyyMMdd").parse(versionProps.getProperty("releasedate")));
		} catch (DateTimeParseException e) {
			LOG.atError().log("Release Datum nicht gesetzt. (Hinweis: Nur in der Entwicklungsumgebung ist dieser Fehler normal!)");
		} catch (NullPointerException e) {
			LOG.atWarn().log("NPE beim Lesen des Releasedate");
		} catch (Exception e) {
			LOG.atWarn().log("Exception beim Lesen des Releasedate" + e.getMessage());
		}
	}

	/**
	 * Gibt ein als "dd.MM.yyyy"-formattiertes Release-Date zurueck.
	 * @return das Release Date als formattierter String
	 */
	public static String getReleaseDate() {
		if (releaseDate == null) return "";
		return new SimpleDateFormat("dd.MM.yyyy").format(releaseDate);
	}

	/**
	 * Gibt das Release Date formattiert fuer den HTML Meta-Tag zurueck, z.B. 2005-08-09.
	 *
	 * @return das Release Date fuer den HTML-Meta-Tag
	 */
	public static String getHtmlMetaDate() {
		if (releaseDate == null) return "";
		return new SimpleDateFormat("yyyy-MM-dd").format(releaseDate);
	}

	/**
	 * Gibt das Jahr des Releases zurueck. 
	 * 
	 * @return das Jahr
	 */
	public static String getYear() {
		if (releaseDate == null) return "";
		return String.valueOf(releaseDate.getYear());
	}

	/**
	 * Gibt den namen der Applikation und die Versionsnummer und das Release-Datum zurueck.
	 * @return die volle Spezifikation der Applikation
	 */
	public static String getFullId() {
		return APPLICATION + " " + VERSION + " " + getReleaseDate();
	}

	/**
	 * Gibt die Versionskennzeichnung als String zurueck.
	 * @return die Versionskennzeichnung
	 */
	public static String getVersion() {
		return VERSION;
	}


}
