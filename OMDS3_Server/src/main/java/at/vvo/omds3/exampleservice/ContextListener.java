package at.vvo.omds3.exampleservice;


import java.util.TimeZone;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import at.vvo.omds3.exampleservice.config.AppContext;
import at.vvo.omds3.exampleservice.fake.jobs.ModelScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Der Context-Listener wird gerufen, wenn die Applikation startet 
 * und wenn die Applikation runter gefahren wird.
 * 
 * 
 */
public class ContextListener implements ServletContextListener {
	
	private static final Logger LOG = LoggerFactory.getLogger(ContextListener.class);
	
	 /* This method is called when the servlet context is
    initialized(when the Web application is deployed). 
    You can initialize servlet context related data here.
 */ 
    @Override
	public void contextInitialized(ServletContextEvent event) {
    	
    	TimeZone.setDefault(TimeZone.getTimeZone("Europe/Vienna")); 
    	
    	LOG.atInfo().log("slf-Logger Info-Level: Context-Listenter: Applikation gestartet");
    	
    	String str = event.getServletContext().getServletContextName();
    	LOG.atInfo().log("Servlet-Context-Name: " + str);
    	ModelScheduler.start();
    }
 
    
    @Override
	public void contextDestroyed(ServletContextEvent event) {
    	LOG.atInfo().log("Context-Listenter: Applikation beendet");
    	
    	// Versuche den Scheduler zu stoppen
		try {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.shutdown(true);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}

		
      /* This method is invoked when the Servlet Context 
         (the Web application) is undeployed or 
         WebLogic Server shuts down.
      */			    
    	//close resources
    	AppContext.getInstance().close();   
    }
}