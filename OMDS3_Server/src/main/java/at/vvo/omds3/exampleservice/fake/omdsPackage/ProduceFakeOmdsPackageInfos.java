package at.vvo.omds3.exampleservice.fake.omdsPackage;

import at.vvo.omds.types.omds2Types.v2_16.PaketInhCdType;
import at.vvo.omds.types.omds2Types.v2_16.PaketUmfCdType;
import at.vvo.omds3.exampleservice.config.AppContext;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class ProduceFakeOmdsPackageInfos {
	
	private static final DecimalFormat VUNR_FORMAT = new DecimalFormat("0000");


	static List<OmdsPackage> fakeOmdsPackageInfoListe() {
		ArrayList<OmdsPackage> liste = new ArrayList<>();
		
		// lese VUNr aus Config und formattiere sie
		int vunr = 0;
		vunr = AppContext.getAppConfig().getVunr();
		String vunrAlsString = VUNR_FORMAT.format(vunr);
		
		for (int i = 0; i < 10; i++) {
			liste.add(createFakeOmdsPackageInfo(VUNR_FORMAT.format(444), "98765432"));
		}
		
		return liste;
	}
	
	private static OmdsPackage createFakeOmdsPackageInfo(String vuNr, String maklerID) {
		LocalDateTime jetzt = LocalDateTime.now();

		Random randomGenerator = new Random();
		
		String id = Integer.toString(Math.abs(randomGenerator.nextInt()));
		LocalDateTime idValidUntil = jetzt.plusHours(1);
		boolean singleUse = false;

		int packageSizeInBytes = Math.abs(randomGenerator.nextInt());
		PaketUmfCdType packageExtent = createRandomPackageExtent();
		List<PaketInhCdType> packageContentCodes = createFakePackageContentCodes();
		return new OmdsPackage(
				vuNr, maklerID, new OmdsPackageIdDetails(id, idValidUntil, singleUse), 
				jetzt, packageSizeInBytes, packageExtent, packageContentCodes);
		
	}
	
	private static PaketUmfCdType createRandomPackageExtent() {
		Random randomGenerator = new Random();
		PaketUmfCdType packageExtent = (randomGenerator.nextBoolean()) ? PaketUmfCdType.D : PaketUmfCdType.G;
		return packageExtent;
	}
	
/**
 * <xsd:restriction base="xsd:string">
			<xsd:enumeration value="AI">
				<xsd:annotation>
					<xsd:documentation>Allgem. Initialbestand (generelle Schlüssel)</xsd:documentation>
				</xsd:annotation>
			</xsd:enumeration>
			<xsd:enumeration value="VF">
				<xsd:annotation>
					<xsd:documentation>VU Fondsbestand</xsd:documentation>
				</xsd:annotation>
			</xsd:enumeration>
			<xsd:enumeration value="VI">
				<xsd:annotation>
					<xsd:documentation>VU Initialbestand (VU Schlüssel)</xsd:documentation>
				</xsd:annotation>
			</xsd:enumeration>
			<xsd:enumeration value="VK">
				<xsd:annotation>
					<xsd:documentation>VU Mahn/Klagebestand</xsd:documentation>
				</xsd:annotation>
			</xsd:enumeration>
			<xsd:enumeration value="VM">
				<xsd:annotation>
					<xsd:documentation>VU Mischbestand</xsd:documentation>
				</xsd:annotation>
			</xsd:enumeration>
			<xsd:enumeration value="VP">
				<xsd:annotation>
					<xsd:documentation>VU Provisionen</xsd:documentation>
				</xsd:annotation>
			</xsd:enumeration>
			<xsd:enumeration value="VS">
				<xsd:annotation>
					<xsd:documentation>VU Schadenbestand</xsd:documentation>
				</xsd:annotation>
			</xsd:enumeration>
			<xsd:enumeration value="VV">
				<xsd:annotation>
					<xsd:documentation>VU Vertragsbestand</xsd:documentation>
				</xsd:annotation>
			</xsd:enumeration>
		</xsd:restriction>
 * @return
 */
	private static List<PaketInhCdType> createFakePackageContentCodes() {
		Random randomGenerator = new Random();
		int anzahl = randomGenerator.nextInt(8) + 1; // 1 .. 8 Codes koennen auftreten
		
		SortedSet<Integer> codes = new TreeSet<>();
		while (codes.size() < anzahl) {
			int code = randomGenerator.nextInt(8); // erzeuge einen Code 0 .. 7
			codes.add(code);
		}
		
		List<PaketInhCdType> liste = new ArrayList<>();
		for (Iterator<Integer> iterator = codes.iterator(); iterator.hasNext();) {
			Integer code = (Integer) iterator.next();
			liste.add(erzSchluessel(code));
		}
		return liste;
	}
	
	private static PaketInhCdType erzSchluessel(int code) {
		switch (code) {
		default:
		case 0:
			return PaketInhCdType.AI;
		case 1:
			return PaketInhCdType.VF;
		case 2:
			return PaketInhCdType.VI;
		case 3:
			return PaketInhCdType.VK;
		case 4:
			return PaketInhCdType.VM;
		case 5:
			return PaketInhCdType.VP;
		case 6:
			return PaketInhCdType.VS;
		case 7:
			return PaketInhCdType.VV;
		}
	}
}
