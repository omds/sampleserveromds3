package at.vvo.omds3.exampleservice.fake.omdsPackage;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import at.vvo.omds.types.omds2Types.v2_16.PAKET;
import at.vvo.omds.types.omds2Types.v2_16.PERSONType;
import at.vvo.omds.types.omds2Types.v2_16.PaketInhCdType;
import at.vvo.omds.types.omds2Types.v2_16.PaketUmfCdType;
import at.vvo.omds.types.omds2Types.v2_16.VERTRAGType;
import at.vvo.omds3.exampleservice.config.AppContext;
import at.vvo.omds3.exampleservice.fake.StringUtils;

/**
 * Packt das Modell fuer eine MaklerID in ein OMDS-Paket.
 * 
 * @author Jens
 *
 */
public class ErzeugerBestandsPackage {
	
	/**
	 * Erzeuge einen OMDS Datensatz fuer eine MaklerID
	 * @param maklerID
	 * @return
	 */
//	public static OMDS erzeugeOMDSDatensatz(ModelForMaklerID modelForMaklerID, Map<String, PERSONType> mapPersonen) {
//		OMDS omds = new OMDS();
//		omds.setVersion(AppContext.getAppConfig().getOmdsVersion());
//		
//		ergaenzePakete(omds, modelForMaklerID, mapPersonen);
//		
//		return omds;
//	}
	
	/** 
	 * Ergaenzt den uebergebenen Datensatz um Pakete fuer die MaklerID.
	 * In einem Paket sind immer die Daten für eine Vermittlernummer.
	 * 
	 */
//	private static void ergaenzePakete(OMDS omds, ModelForMaklerID modelForMaklerID, Map<String, PERSONType> mapPersonen) {
//		int n = 1;
//		for (String vermnr : modelForMaklerID.getMapContentByVermnr().keySet()) {
//			ContentForVermittlernr modellVermnr = modelForMaklerID.getContentByVermnr(vermnr);
//		
//			PAKET paket = erzeugePaket(n, vermnr, mapPersonen, modellVermnr.getMapVertraege().values());
//			
//			// haenge es in den Datensatz
//			omds.getPAKET().add(paket);
//			n++;
//		}
//		
//	}
	
	/**
	 * Erzeugt ein Paket fuer jede Vermittlernr
	 * @param lfdNrPaket
	 * @param maklerID
	 * @return
	 */
	private static PAKET erzeugePaket(
			int lfdNrPaket, 
			String maklerID, 
			Map<String, PERSONType> mapPersonen, 
			Collection<VERTRAGType> listeVertraegeVermittler
	) {
		
		
		PAKET p = new PAKET();
		p.setDVRNrAbs(AppContext.getAppConfig().getDvrNr()); // 1-8 Stellen
		p.setMaklerID(maklerID);
		p.setOMDSVersion(AppContext.getAppConfig().getOmdsVersion());
		p.setPaketKommentar("Dies ist Paket Nr " + lfdNrPaket + " für MaklerID " + maklerID);
		LocalDateTime erstellungsZpkt = LocalDateTime.now();
		LocalDateTime einMonatZuvor = erstellungsZpkt.minusMonths(1);
		p.setPaketZpktErstell(StringUtils.dateToXmlGreogrianCalendar(erstellungsZpkt));
		p.setPaketZpktLetztErstell(StringUtils.dateToXmlGreogrianCalendar(einMonatZuvor));
		p.setVUNr(StringUtils.VUNR_FORMAT.format(AppContext.getAppConfig().getVunr()));
		p.setVUVersion(""); // was kommt da rein?,  max 12 Stellen

		// in the Package XOR
		// - VERS_UNTERNEHMEN + SCHLUESSELART, InhCd = AI (Allgemeiner Initialbestand: generelle Schlüssel)
		// - KLAUSEL, InhCd = VI - VU Initialbestand (VU Schlüssel)
		// - group:Bestand InhCd = VM (VU Mischbestand), VV (VU Vertragsbestand), VS (VU Schadenbestand), VP (VU Provisionen), VK (VU Mahn/Klagebestand), VF (VU Fondsbestand)
		//      - LOESCHANSTOSS
		//      - PERSON
		//      - VERTRAG
		//      - SCHADEN
		//      - PROVISION
		//      - MAHNUNG
		//      - VERTRAGSFONDS
		
		p.setPaketInhCd(PaketInhCdType.VV); // Vertragsbestand
		p.setPaketUmfCd(PaketUmfCdType.G); // D = Differenzbestand, G = Gesamtbestand
		//TODO p.getPERSON().addAll(c)
		//TODO p.getVERTRAG().addAll(listeVertraegeVermittler);
		for (Iterator<VERTRAGType> iterator = listeVertraegeVermittler.iterator(); iterator.hasNext();) {
			VERTRAGType vertragType = (VERTRAGType) iterator.next();
			//p.getVERTRAG().addAll(listeVertraegeVermittler);
		}
		return p;
	}


}
