package at.vvo.omds3.exampleservice.fake.polDocuments;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;


public class MCArcImageInfo implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	
	protected MCElementIdTypeModel arcImageIdDetails;
    protected String name;
    protected int documentType;
    protected String arcContentType;
    protected Long arcContentLength;
    protected LocalDateTime date;
    protected List<MCArcImageInfo> docReference;
    
	public MCElementIdTypeModel getArcImageIdDetails() {
		return arcImageIdDetails;
	}
	public void setArcImageIdDetails(MCElementIdTypeModel arcImageIdDetails) {
		this.arcImageIdDetails = arcImageIdDetails;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDocumentType() {
		return documentType;
	}
	public void setDocumentType(int documentType) {
		this.documentType = documentType;
	}
	public String getArcContentType() {
		return arcContentType;
	}
	public void setArcContentType(String arcContentType) {
		this.arcContentType = arcContentType;
	}
	public Long getArcContentLength() {
		return arcContentLength;
	}
	public void setArcContentLength(Long arcContentLength) {
		this.arcContentLength = arcContentLength;
	}
	
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	public List<MCArcImageInfo> getDocReference() {
		return docReference;
	}
	public void setDocReference(List<MCArcImageInfo> docReference) {
		this.docReference = docReference;
	}
    
    
    
    

}
