package at.vvo.omds3.exampleservice.fake;

import at.vvo.omds.types.omds2Types.v2_16.*;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ElementIdType;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.OMDSPackageInfoType;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.RequestedOMDSPackage;
import at.vvo.omds3.exampleservice.config.AppContext;
import at.vvo.omds3.exampleservice.config.MaklerID;
import at.vvo.omds3.exampleservice.fake.omdsPackage.RandomUtils;
import jakarta.activation.DataHandler;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;



/**
 * Enthaelt das Modell fuer eine MaklerID.
 * 
 * @author Jens
 *
 */
public class ModelForMaklerID {
	
	private static final Logger LOG = LoggerFactory.getLogger(ModelForMaklerID.class);
	
	private final MaklerID maklerID;
	
	private Map<String, ModelVermittlernr> mapContentByVermnr;
	Date letzteErstellungOMDSPaket; // fuer MaklerID
	
	//List<OMDSPackageInfoType> aktuelleListeOMDSDatensaetzeInfo;

	/**
	 * Constructor, erzeugt ein Modell fuer diese MaklerID.
	 * @param maklerID die MaklerID
	 */
	public ModelForMaklerID(MaklerID maklerID, Map<String, PERSONType> personen, Map<String, VERTRAG> vertraege, int minAnz, int maxAnz) {
		this.maklerID = maklerID;
		
		// erzeuge Modell je Vermittlernummer
		this.mapContentByVermnr = new HashMap<>();
		//this.aktuelleListeOMDSDatensaetzeInfo = new ArrayList<>();
		fuegeVertraegeHinzu(personen, vertraege, minAnz, maxAnz);
	}

	public MaklerID getMaklerID() {
		return maklerID;
	}

	public ModelVermittlernr getContentByVermnr(String vermnr) {
		return mapContentByVermnr.get(vermnr);
	}

	public Map<String, ModelVermittlernr> getMapContentByVermnr() {
		return mapContentByVermnr;
	}

	public void fuegeVertraegeHinzu(Map<String, PERSONType> personen, Map<String, VERTRAG> vertraege, int minAnz, int maxAnz) {
		
		for (String vermnr : maklerID.getVermnr()) {
			
			if (mapContentByVermnr.containsKey(vermnr)) {
				ModelVermittlernr contentVermittler = mapContentByVermnr.get(vermnr);
				contentVermittler.fuegeVertraegeHinzu(personen, vertraege, minAnz, maxAnz);
			} else {
				ModelVermittlernr contentVermittler = new ModelVermittlernr(maklerID.getMaklerID(), vermnr, personen, vertraege, minAnz, maxAnz);
				mapContentByVermnr.put(vermnr, contentVermittler);
			}
		}
	}
	
	/**
	 * Erzeugt einen neuen OMDS Datensatz mit PAKETen fuer jede Vermittlernummer
	 * @param mapOMDSPacketById 
	 * @param paketUmf
	 * @return OMDS als OMDSPackageInfoType
	 */
	public RequestedOMDSPackage erzeugeOMDSDatensatzUndInfo(Map<String, RequestedOMDSPackage> mapOMDSPacketById, PaketUmfCdType paketUmf) {
		Date datumDerErstellung = new Date();
		OMDS omds = erzeugeOMDSDatensatz(paketUmf, datumDerErstellung);
		
		DataHandler payload = null;
		int packageSize = 0;
		try {
			byte[] marshalledOMDS = marshalOMDSDatensatz(omds);

			//logger.debug("Marshalled: " + new String(marshalledOMDS).toString());
			byte[] zipped = ZipUtils.zip("OMDS-Datensatz", marshalledOMDS);
			packageSize = zipped.length;
			//logger.debug("Zipped: " + new String(zipped).toString());
			//byte[] base64EncodedData = Base64.encodeBase64(zipped);
			//logger.debug("Base64 Encoded: " + new String(base64EncodedData).toString());
			//payload = base64EncodedData;
			payload = new DataHandler(zipped, "application/zip");
		} catch (JAXBException e) {
			LOG.atWarn().log("Could not marshal OMDSDatensatz for " + maklerID.toString());
			e.printStackTrace();
		} catch (IOException e) {
			LOG.atWarn().log("Could not zip OMDSDatensatz for " + maklerID.toString());
			e.printStackTrace();
		}
		
		OMDSPackageInfoType packageInfo = new OMDSPackageInfoType();
		
		packageInfo.setMaklerID(maklerID.getMaklerID());
		
		// erzeuge Id
		ElementIdType eid = new ElementIdType();
		eid.setId(erzeugeDatensatzId(mapOMDSPacketById.keySet()));
		eid.setIdIsSingleUse(false);
		LocalDateTime validUntil = LocalDateTime.now().plusMinutes(AppContext.getAppConfig().getMinutesOMDSDatensatzValid());
		eid.setIdValidUntil(StringUtils.dateToXmlGreogrianCalendar(validUntil));
		packageInfo.setOmdsPackageIdDetails(eid);
		
		// weitere Merkmale
		packageInfo.setPackageExtent(paketUmf);
		packageInfo.setPackageSize(packageSize);
		packageInfo.setTimeStamp(StringUtils.dateToXmlGreogrianCalendar(datumDerErstellung));
		packageInfo.setVUNr(StringUtils.VUNR_FORMAT.format(AppContext.getAppConfig().getVunr()));
		
		RequestedOMDSPackage requestedPackage = new RequestedOMDSPackage();
		RequestedOMDSPackage.OmdsPackage op = new RequestedOMDSPackage.OmdsPackage();
		op.setContent(payload);
		op.setOmdsPackageInfo(packageInfo);
		requestedPackage.setOmdsPackage(op);
		requestedPackage.setRequestedOmdsPackageId(eid.getId());
		
		// registiere neuen Datensatz unter der Id im Modell
		mapOMDSPacketById.put(eid.getId(), requestedPackage);
		
		// ergaenze die Liste fuer diese MaklerID um den neuen Datensatz
		//aktuelleListeOMDSDatensaetzeInfo.add(packageInfo);
		
		return requestedPackage;
	}

	/** erzeugt ein STRING aus dem OMDS Datensatzobjekt */
	private byte[] marshalOMDSDatensatz(OMDS omds) throws JAXBException {
		
		JAXBContext jaxbContext = JAXBContext.newInstance(OMDS.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		StringWriter sw = new StringWriter();
		
		jaxbMarshaller.marshal(omds, sw);
		
		String xmlString = sw.toString();
		LOG.debug("OMDS Datensatz als String:" + xmlString);

        return xmlString.getBytes();
	}

	/**
	 * Erzeugt eine neue Personennummer, die im Modell noch nicht vergeben ist.
	 * @param bestehendeIds das Set der schon vergebenen Personennummern
	 * @return die neue Nummer
	 */
	private static String erzeugeDatensatzId(Set<String> bestehendeIds) {
		int min = AppContext.getAppConfig().getMinPackageNr();
		int max = AppContext.getAppConfig().getMaxPackageNr();
		return RandomUtils.createNr(min, max, bestehendeIds);
	}
	
	private OMDS erzeugeOMDSDatensatz(PaketUmfCdType paketUmf, Date datumDerErstellung) {
		Set<String> mengeVermnr = mapContentByVermnr.keySet();
		
		
		OMDS omdsDatensatz = new OMDS();
		//dsDatensatz.setVersion(AppContext.getAppConfig().getOmdsVersion());
		
		// iteriere die Vermittlernummern, die in diesem Paket zusammengefasst werden
		for (String vermnr : mengeVermnr) {
			ModelVermittlernr contentVermnr = mapContentByVermnr.get(vermnr);
			
			PAKET p = erzeugeOMDSPaketFuerVermittlernr(contentVermnr, paketUmf, datumDerErstellung);
			omdsDatensatz.getPAKET().add(p);
		} 
		
		letzteErstellungOMDSPaket = datumDerErstellung;
		return omdsDatensatz;
	}
	
	/** Erzeuge neues Paket fuer eine Vermittlernummer */
	private PAKET erzeugeOMDSPaketFuerVermittlernr(ModelVermittlernr modelVermnr, PaketUmfCdType paketUmf, Date datumDerErstellung) {

		PAKET p = new PAKET();
		p.setDVRNrAbs(AppContext.getAppConfig().getDvrNr());
		p.setMaklerID(maklerID.getMaklerID());
		p.setOMDSVersion(AppContext.getAppConfig().getOmdsVersion());
		p.setPaketInhCd(PaketInhCdType.VM);
		p.setPaketKommentar("Paket fuer MaklerID " + maklerID.getMaklerID() + ", Vermittlernummer " + modelVermnr.getVermnr());
		p.setPaketUmfCd(paketUmf);
		p.setPaketZpktErstell(StringUtils.dateToXmlGreogrianCalendar(datumDerErstellung));
		if (letzteErstellungOMDSPaket != null)
			p.setPaketZpktLetztErstell(StringUtils.dateToXmlGreogrianCalendar(letzteErstellungOMDSPaket));
		
		p.setVUNr(StringUtils.VUNR_FORMAT.format(AppContext.getAppConfig().getVunr()));
		//p.setVUVersion(value); //TODO was waere das?

		p.getVERTRAG().addAll(modelVermnr.getListeVertraegeDieserVermittlernr());

		return p;
	}
	
	public Date getLetzteErstellungOMDSPaket() {
		return letzteErstellungOMDSPaket;
	}
}
