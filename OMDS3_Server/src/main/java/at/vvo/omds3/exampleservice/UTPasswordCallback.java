package at.vvo.omds3.exampleservice;

import java.io.IOException;

import at.vvo.omds3.exampleservice.config.User;
import at.vvo.omds3.exampleservice.fake.VuModel;
import org.apache.ws.security.WSPasswordCallback;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * Prueft die Passwoerter und haelt sie in einer Map
 * @author Jens Buehring
 *
 */
public class UTPasswordCallback implements CallbackHandler {
	
	
	/**
	 * Here, we attempt to get the password from the private
	 * alias/passwords map.
	 */
	@Override
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
		for (int i = 0; i < callbacks.length; i++) {

			WSPasswordCallback pc = (WSPasswordCallback) callbacks[i];

			String username = pc.getIdentifier();

			// setze das geforderte Passwort im PasswordCallback
			User u = VuModel.getInstance().getUsersByUsername().get(username);
			if (u != null && u.getPasswort() != null && u.getPasswort().length() > 0) {
				pc.setPassword(u.getPasswort());
				return;
			}


		}
	}

//	/**
//	 * Add an alias/password pair to the callback mechanism.
//	 */
//	public void setAliasPassword(String alias, String password) {
//		passwords.put(alias, password);
//	}
}
