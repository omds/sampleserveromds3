package at.vvo.omds3.exampleservice.fake.polDocuments;

import java.io.Serializable;
import java.util.Date;


public class MCElementIdTypeModel implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected String id;
    protected Date idValidUntil;
    protected boolean idIsSingleUse;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isIdIsSingleUse() {
		return idIsSingleUse;
	}
	public void setIdIsSingleUse(boolean idIsSingleUse) {
		this.idIsSingleUse = idIsSingleUse;
	}
	public Date getIdValidUntil() {
		return idValidUntil;
	}
	public void setIdValidUntil(Date idValidUntil) {
		this.idValidUntil = idValidUntil;
	}
	
	
    
    

}
