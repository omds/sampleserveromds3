package at.vvo.omds3.exampleservice.fake;

import at.vvo.omds.types.omds2Types.v2_16.PERSONType;
import at.vvo.omds.types.omds2Types.v2_16.PaketUmfCdType;
import at.vvo.omds.types.omds2Types.v2_16.VERTRAG;
import at.vvo.omds.types.omds3Types.r1_11_0.common.AgentFilterType;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.DeepLinkBusinessObjectResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.HttpActionLinkType;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.OMDSPackageInfoType;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.RequestedOMDSPackage;
import at.vvo.omds3.exampleservice.config.AppContext;
import at.vvo.omds3.exampleservice.config.MaklerID;
import at.vvo.omds3.exampleservice.config.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Diese Singleton-Klasse ist der Kern der simulierten Versicherung.
 * Sie hält alle Daten der simulierten Versicherung.
 * @author Jens Buehring
 *
 */
public class VuModel {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(VuModel.class);
	
	// the instance
	private static VuModel instance;
	
	// Content
	private Map<String, User> usersByUsername;
	
	private Map<String, ModelForUser> contentForUsername;
	private Map<String, PERSONType> personen;
	private Map<String, VERTRAG> vertraege;
	
	private List<HttpActionLinkType> listePortalLoginLinks;

	private Map<String, RequestedOMDSPackage> mapOMDSDatensaetzeById;
	
	/**
	 * Dieser private Constructor erstellt die fiktive Versicherung erstmalig.
	 */
	private VuModel() {
		initModel();
	}
	
	/**
	 * Initialisiert das Modell.
	 * Diese Methode wird beim ersten Zugriff gerufen, aber auch über die Methode cleanModel.
	 */
	private void initModel() {
		this.contentForUsername = new HashMap<>();
		this.personen = new HashMap<>();
		this.vertraege = new HashMap<>();
		this.mapOMDSDatensaetzeById = new HashMap<>();
		//this.mapOMDSPackagesByUsername = new HashMap<>();
		
		usersByUsername = loadUsersFromSpring();
		
		erzeugeVertraege(AppContext.getAppConfig().getMinAnzVertraegeInit(), AppContext.getAppConfig().getMaxAnzVertraegeInit());
		
		listePortalLoginLinks = erzeugeListeHttpActionLinks();
		
	}
	
	private List<HttpActionLinkType> erzeugeListeHttpActionLinks() {
		
		List<HttpActionLinkType> liste = new ArrayList<>();
		
		HttpActionLinkType login = new HttpActionLinkType();
		login.setValidUntil(StringUtils.toXMLGregorianCalendar(LocalDateTime.now().plusHours(24)));
		login.setUrl("https://www.kapdion.com/");
		login.setType(1);
		liste.add(login);
		return liste;
	}
	

	/**
	 * Gibt das VuModel zurueck. Sollte noch keins existieren,
	 * erzeugt die Methode ein neues VuModel und haelt es im Speicher.
	 * @return das VuModel
	 */
	public static VuModel getInstance() {
		if (instance == null)
			instance = new VuModel();
		return instance;
	}


	/**
	 * Bezieht die User aus der Spring-Configuration
	 * @return die Map der User nach Username
	 */
	private static Map<String, User> loadUsersFromSpring() {
		List<User> liste = AppContext.getAppConfig().getListUsers();
		Map<String, User> ergebnis = new HashMap<>();
		
		for (User user : liste) {
			if (user != null)
				ergebnis.put(user.getUsername(), user);
		}
		
		return ergebnis;
	}

	public Map<String, ModelForUser> getContentForUsername() {
		return contentForUsername;
	}

	public Map<String, PERSONType> getPersonen() {
		return personen;
	}
	
	/**
	 * Gibt eine Map der User nach Username zurueck.
	 * Diese Map wird über die Spring-Config definiert.
	 * @return die Map der User
	 */
	public Map<String, User> getUsersByUsername() {
		return usersByUsername;
	}


	public DeepLinkBusinessObjectResponse getLogin(String username) {
		
		//TODO pruefe Username, lege session an
		
		DeepLinkBusinessObjectResponse r = new DeepLinkBusinessObjectResponse();
		r.getHttpActionLink().addAll(listePortalLoginLinks);
		return r;
	}

	public Map<String, VERTRAG> getVertraege() {
		return vertraege;
	}
	

	public List<OMDSPackageInfoType> getListeOmdsDatensaetze(String username, MaklerID maklerID) {
		ModelForUser mu = contentForUsername.get(username);
		List<OMDSPackageInfoType> liste = mu.getListeOMDSDatensaetze(maklerID);
		return liste;
	}
	
	/**
	 * Metode erzeugt neue Vertrage und dann erstellt sie neue OMDS-Packages
	 */
	public void createAdditionalPolicies() {
		LOGGER.atDebug().log("erzeuge neue Vertraege");
		erzeugeVertraege(AppContext.getAppConfig().getMinAnzVertraegeErg(), AppContext.getAppConfig().getMaxAnzVertraegeErg());
		erzeugeOMDSDatensaetze();
		entferneAlteOMDSDatensaetze();
	}
	
	

	private void entferneAlteOMDSDatensaetze() {
		Date now = new Date();
		Collection<ModelForUser> modelle = contentForUsername.values();
		for (ModelForUser m : modelle) {
			m.entferneAlteOMDSDatensaetze(mapOMDSDatensaetzeById, now);
		}
		
	}

	private void erzeugeOMDSDatensaetze() {
		// Erzeugt OMDS-Datensaetze
		for (String username : contentForUsername.keySet()) {
			ModelForUser m = contentForUsername.get(username);
			m.erzeugeOMDSDatensaetze(mapOMDSDatensaetzeById, PaketUmfCdType.G);
		}
		
	}

	private void erzeugeVertraege(int vonAnz, int bisAnz) {
		for (String username : usersByUsername.keySet()) {
			// fuer jede MaklerID werden Vertraege und Claims erzeugt
			User u = usersByUsername.get(username);
			
			if (!contentForUsername.containsKey(username)) {
				contentForUsername.put(username, new ModelForUser(u, personen, vertraege, vonAnz, bisAnz));
			} else {
				ModelForUser m = contentForUsername.get(username);
				m.fuegeVertraegeHinzu(personen, vertraege, vonAnz, bisAnz);
			}
		}
	}

	/**
	 * Liefert spezifizierte OMDS-Datensaetze zurueck.
	 * @param vuNr die VUNr
	 * @param agentFilterType eventuell ein agentFilter
	 * @param omdsPackageId die Liste der omdsDatensaetze
	 */
	public List<RequestedOMDSPackage> getOMDSDatensaetzeById(
			String username, 
			MaklerID maklerID, 
			String vuNr, 
			AgentFilterType agentFilterType,
			List<String> omdsPackageId
	) {
		List<RequestedOMDSPackage> ergebnisListe = new ArrayList<>();
		// TODO Filter implementieren
		for (String id : omdsPackageId) {
			ergebnisListe.add(mapOMDSDatensaetzeById.get(id));
		}
		return ergebnisListe;
	}

	public void cleanModel() {
		LOGGER.atDebug().log("Complete Clean of VU-Model");
		initModel();
	}

}
