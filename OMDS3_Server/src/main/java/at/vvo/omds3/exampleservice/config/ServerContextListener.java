package at.vvo.omds3.exampleservice.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.TimeZone;
import java.util.logging.Handler;
import java.util.logging.Level;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;




/**
 * Der Server-Context-Listener wird gerufen, wenn die Applikation startet 
 * und wenn die Applikation runter gefahren wird.
 * 
 * 
 */
public class ServerContextListener implements ServletContextListener {
	
	private static final java.util.logging.Logger JUL_LOGGER =
			java.util.logging.Logger.getLogger(ServletContextListener.class.getName());
	private static final Logger LOGGER = LoggerFactory.getLogger(ServerContextListener.class);
	
	 /* This method is called when the servlet context is initialized (when the Web application is deployed).
    You can initialize servlet context related data here. */
    public void contextInitialized(ServletContextEvent event) {
    	
    	JUL_LOGGER.fine("setze Timezone");
    	TimeZone.setDefault(TimeZone.getTimeZone("Europe/Vienna")); //"Etc/UTC"));
    	
    	Handler[] h = JUL_LOGGER.getHandlers();
        for (Handler handler : h) {
            handler.setLevel(Level.FINE);
        }

    	
    	JUL_LOGGER.setLevel(Level.FINE);
    	JUL_LOGGER.info("Jul-Logger Info-Level: Context-Listenter: Server gestartet");
    	LOGGER.atInfo().log("Log4j-Logger Info-Level: Context-Listenter: Server gestartet");
    	//ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-configuration.xml");

    	String str = event.getServletContext().getServletContextName();
    	JUL_LOGGER.info("Servlet-Context-Name: " + str);

    }
    
    
    public void contextDestroyed(ServletContextEvent event) {
    	JUL_LOGGER.info("Context-Listenter: Applikation beendet");
      /* This method is invoked when the Servlet Context 
         (the Web application) is undeployed or 
         WebLogic Server shuts down.
      */			    
    	//close resources
    	//AppContext.getInstance().close();   
    }
}