/**
 * 
 */
/**
 * Basispackage fuer die gefakten Inhalte, die uber diesen
 * Server in die Welt gesetzt werden sollen.
 * 
 * @author Jens Buehring
 * 
 */
package at.vvo.omds3.exampleservice.fake;