package at.vvo.omds3.exampleservice.fake.omdsPackage;

import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.OMDSPackageInfoType;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.OMDSPackageListResponse;

import java.util.List;



/**
 * Implementierung des Response-Objektes OMDSPackageListResponse.
 * @author Jens Buehring
 *
 */
public class OMDSPackageListResponseImpl extends OMDSPackageListResponse {

	public void setOmdsPackageInfo(List<OMDSPackageInfoType> input) {
		omdsPackageInfo = input;
	}
}
