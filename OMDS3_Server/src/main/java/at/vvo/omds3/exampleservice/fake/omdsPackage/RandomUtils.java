package at.vvo.omds3.exampleservice.fake.omdsPackage;

import java.util.Random;
import java.util.Set;

public class RandomUtils {
	
	private static final Random randomGenerator = new Random();
	
	
	
	public static Integer randomOfIntegerArray(Integer[] array) {
		int anz = array.length;
		int n = randomGenerator.nextInt(anz);
		return array[n];
	}
	
	public static String randomOfStringArray(String[] array) {
		int anz = array.length;
		int n = randomGenerator.nextInt(anz);
		return array[n];
	}

	public static boolean nextBoolean() {
		return randomGenerator.nextBoolean();
	}

	public static Object randomOfObjectArray(Object[] array) {
		int anz = array.length;
		int n = randomGenerator.nextInt(anz);
		return array[n];
	}
	
	/**
	 * Erzeugt eine neue Nummer, die im Modell noch nicht vergeben ist.
	 * @param bestehendeNummern das Set der schon vergebenen Nummern
	 * @return die neue Nummer
	 */
	public static String createNr(int min, int max, Set<String> bestehendeNummern) {
		String nr = null;
		while (nr == null || bestehendeNummern.contains(nr)) {
			nr = String.valueOf(randomGenerator.nextInt(max - min) + min);
		}
		return nr;
	}

}
