package at.vvo.omds3.exampleservice.fake.configuration;

import at.vvo.omds.types.omds2Types.v2_16.ADRESSEType;
import at.vvo.omds.types.omds2Types.v2_16.NATUERLICHEPERSONType;
import at.vvo.omds.types.omds2Types.v2_16.PERSONType;
import at.vvo.omds.types.omds2Types.v2_16.PersArtCdType;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.UserDataResponse;
import at.vvo.omds3.exampleservice.config.User;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Jens Buehring
 *
 */
public class WrapperUserForSoapTransport {
	
	/**
	 * Erzeuge ein UserDataRespons aus einem User-Objekt, welches z.B. aus
	 * der Spring-Configuration stammen kann.
	 * @param u der user
 	 * @return das UserDataResponse-Objekt
	 */
	public static UserDataResponse initializeUser(User u) {
		UserDataResponse r = new UserDataResponse();
		
		r.setPerson(createPerson(u));
		//r.setAddress(createAddress(u));
		r.setAvailableServices(createAvailableServices(u));
		r.setUserid(u.getUsername());
		
		return r;
	} 
	
	/**
	 * Erzeugt das Rueckgabeobjekt fuer die verfuegbaren Services.
	 * @param u der User
	 * @return das Rueckgabeobjekt
	 */
	private static UserDataResponse.AvailableServices createAvailableServices(User u) {
		if (u == null || u.getServices() == null)
			return null;
		
		UserDataResponse.AvailableServices as = new UserDataResponse.AvailableServices();
		List<String> services = u.getServices();
		ArrayList<String> result = new ArrayList<>();
        for (String s : services) {
            result.add(s.trim());
        }
		
		as.getService().addAll(result);
		return as;
	}



	/**
	 * Erzeugt das Rueckgabeobjekt fuer die Adresse
	 * @param u das User-Objekt
	 * @return die Adresse
	 */
	private static ADRESSEType createAddress(User u) {
		if (u == null)
			return null;
		
		ADRESSEType a = new ADRESSEType();
		a.setHausnr(u.getHausnr().trim());
		a.setLandesCd(u.getLandesCd().trim());
		a.setOrt(u.getOrt().trim());
		a.setPac(u.getPac());
		a.setPLZ(u.getPlz().trim());
		a.setStrasse(u.getStrasse().trim());
		a.setZusatz(u.getZusatz().trim());
		return a;
	}



	/**
	 * Erzeugt das Rueckgabeobjekt fuer die Person
	 * @param u das User-Objekt 
	 * @return das Person-Rueckgabeobjekt
	 */
	private static PERSONType createPerson(User u) {
		if (u == null)
			return null;
		
		PERSONType r = new PERSONType();
		r.setHausnr(u.getHausnr().trim());
		r.setLandesCd(u.getLandesCd().trim());
		r.setOrt(u.getOrt().trim());
		r.setPac(u.getPac());
		r.setPLZ(u.getPlz().trim());
		r.setStrasse(u.getStrasse().trim());
		r.setZusatz(u.getZusatz().trim());
		
		r.setPersonennr(u.getPersonennr());
		
		r.setNATUERLICHEPERSON(createNatPerson(u));
		r.setSONSTIGEPERSON(null);
		r.setPersArtCd(PersArtCdType.N);
		
		return r;
	}




	private static NATUERLICHEPERSONType createNatPerson(User u) {
		if (u == null)
			return null;
		
		NATUERLICHEPERSONType r = new NATUERLICHEPERSONType();
		r.setFamilienname(u.getFamilienname().trim());
		r.setVorname(u.getVorname().trim());
		
		r.setFamilienstandCd(null);
		r.setGebdat(null);
		r.setLandesCd(null);
		r.setGeschlechtCd(u.getGeschlecht().trim());
		return r;
	}



	
	

}
