package at.vvo.omds3.exampleservice.fake.polDocuments;

import at.vvo.omds.types.omds3Types.r1_11_0.common.AuthorizationFilter;
import at.vvo.omds.types.omds3Types.r1_11_0.common.ElementIdType;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.*;
import at.vvo.omds3.exampleservice.fake.StringUtils;

import jakarta.activation.DataHandler;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class Polizzendokumente {
	
	public static final String MIMETYPE_PDF = "application/pdf";

	private static Polizzendokumente p;
	private Map<String, MCArcContent> mapPolicies;
	private List<MCArcContent> listePolizzenDokumente;
	
	private static final DecimalFormat VUNR_FORMAT = new DecimalFormat("0000");
	
	/**
	 * Privater Constructor
	 */
	private Polizzendokumente() {
		initializePackages();
	}
	
	/**
	 * Erzeugt eine Instance von Connections und gibt diese zurueck.
	 * @return die Instance
	 */
	public static Polizzendokumente getInstance() {
		if(p == null) {
			p = new Polizzendokumente();
		}
		return p;
	}
	

	private void initializePackages() {
		mapPolicies = new HashMap<>();
		listePolizzenDokumente = ErzeugerPolizzendokumente.fakePoliciesListe();

		for(MCArcContent c : listePolizzenDokumente) {
			mapPolicies.put(c.getArcImageInfo().getArcImageIdDetails().getId(), c);
		}
	}
	
	/**
	 * Gibt die Liste der Polizzendokumente zurueck.
	 * @param parameters die Abfrageparameter
	 * @return die Liste
	 */
	public ArcImageInfosResponse getListeArcImageInfos(ArcImageInfosRequest parameters) {
		
		AuthorizationFilter authFilter = parameters.getAuthFilter();
		Date dateFrom = StringUtils.xmlGreogrianCalendarToDate(parameters.getDateFrom());
		Date dateUntil = StringUtils.xmlGreogrianCalendarToDate(parameters.getDateUntil());
		List<Integer> docType = parameters.getDocumentType();
		String polizzennummer = parameters.getPolicyNumber();
		List<PolicyPartnerRole> policyPartyRole = parameters.getPolicyPartnerRole();
		String policyType = parameters.getPolicyType();
		String vunr = parameters.getVUNr();
		
		return KonvertierePolizzenDokumente.konvertiere(listePolizzenDokumente);
	}
	
	
	/* Return all Omds Packages*/
	public Map<String, MCArcContent> getMapPolicies() {
		return mapPolicies; 
	}
	
	
	/* Get Single Omds Package*/
	public ArcImageResponse getPolicyById(String policyId) throws IOException {
	
//		if (getInstance().getMapPolicies() == null)
//			return null;
//		int policyIdConvert = Integer.valueOf(policyId);
		
//		return convertSinglePolicy(getInstance().getMapPolicies().get(VUNR_FORMAT.format(policyIdConvert)));

        return convertSinglePolicy(mapPolicies.get("0001"));
	}
	
	public ArcImageResponse convertSinglePolicy(MCArcContent mcArcImageContent) {
				
		ArcImageResponse resp = new ArcImageResponse();
		
		ArcContent content = new ArcContent();
		
		ArcImageInfo info = new ArcImageInfo();
		info.setArcContentType(MIMETYPE_PDF);
		info.setDocumentType(mcArcImageContent.getArcImageInfo().getDocumentType());
		info.setName(mcArcImageContent.getArcImageInfo().getName());
		info.setArcContentLength(mcArcImageContent.getArcImageInfo().getArcContentLength());
		info.setDate(StringUtils.dateToXmlGreogrianCalendar(StringUtils.stringToDate("05-15-2016")));
		
		ElementIdType type = new ElementIdType();
		type.setId(mcArcImageContent.getArcImageInfo().getArcImageIdDetails().getId());
		type.setIdIsSingleUse(mcArcImageContent.getArcImageInfo().getArcImageIdDetails().isIdIsSingleUse());
		type.setIdValidUntil(null);
		
		info.setArcImageIdDetails(type);
		
		content.setArcImageInfo(info);
		DataHandler dh = new DataHandler(mcArcImageContent.getArcImage(), MIMETYPE_PDF);
		content.setArcImage(dh);
		
		resp.setArcContent(content);
		
		return resp;
	}
	
	
}
