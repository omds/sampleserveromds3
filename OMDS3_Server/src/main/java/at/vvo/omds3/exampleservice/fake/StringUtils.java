/**
 * 
 */
package at.vvo.omds3.exampleservice.fake;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import at.vvo.omds.types.omds3Types.r1_11_0.common.ElementIdType;





/**
 * Utility-Klasse um Strings aus Objekten zu erzeugen.
 * @author Jens Buehring
 *
 */
public final class StringUtils {
	
	public static final Locale AUSTRIA = new Locale("de", "AT");
	public static final DecimalFormatSymbols DFS = new DecimalFormatSymbols(AUSTRIA);
	public static final SimpleDateFormat DATUM = new SimpleDateFormat("dd.MM.yyyy");
	public static final DecimalFormat WAEHRUNG = new DecimalFormat("###,###,##0.00", DFS);
	public static final DecimalFormat BYTE = new DecimalFormat("###,###,##0.0", DFS);
	public static final DecimalFormat DEZIMAL_1_NKS = new DecimalFormat("###,###,##0.0", DFS);
	public static final DecimalFormat WAEHRUNG_OHNE_NKS = new DecimalFormat("###,###,##0", DFS);
	public static final SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	public static final SimpleDateFormat DATE_ONLY = new SimpleDateFormat("dd-MM-yyyy");
	public static final DecimalFormat VUNR_FORMAT = new DecimalFormat("0000");
	

	
	public static LocalDateTime bestimmeFolgendenMonatsersten(LocalDateTime d) {
		int currentMonth = d.getMonthValue();
		return d.withDayOfMonth(1).withMonth(currentMonth);
	}
	
	/**
	 * Wandelt einen Cent in long in Euro in BigDecimal um.
	 * @param cent die Cent
	 * @return die EUR als BigDecimal
	 */
	public static BigDecimal centToEuro(BigDecimal cent) {
		BigDecimal euro = cent.divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
		return euro;
	}

	/**
	 * Wandelt java.util.Date in XMLGregorianCalendar um.
	 * @param date das Datum
	 * @return den Calendar oder null, wenn ein Fehler auftritt
	 */
	public static XMLGregorianCalendar toXMLGregorianCalendar(LocalDateTime date) {
		XMLGregorianCalendar result = null;
        try {
            result = DatatypeFactory.newInstance().newXMLGregorianCalendar(date.toString());
        } catch (DatatypeConfigurationException e) {
			e.printStackTrace();
        }
		return result;
    }

	/**
	 * Wandelt java.util.Date in XMLGregorianCalendar um.
	 * @param date das Datum
	 * @return den Calendar oder null, wenn ein Fehler auftritt
	 */
	public static XMLGregorianCalendar toXMLGregorianCalendar(Date date) {
		
		if (date == null)
			return null;
		
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);
		XMLGregorianCalendar result = null;
		try {
			result = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return result;
	}


	public static XMLGregorianCalendar dateToXmlGreogrianCalendar(LocalDateTime date) {
		return dateToXmlGreogrianCalendar(java.util.Date.from(date
				.atZone(ZoneId.systemDefault())
				.toInstant()));
	}

	public static XMLGregorianCalendar dateToXmlGreogrianCalendar(LocalDate date) {
		return dateToXmlGreogrianCalendar(java.util.Date.from(
				date.atStartOfDay()
						.atZone(ZoneId.systemDefault())
						.toInstant()));
	}

	/**
	 * Erzeugt ein XMLGregorianCalendar aus einem Date.
	 * @param date das Date oder null
	 * @return der XMLGregorianCalendar oder null
	 */
	public static XMLGregorianCalendar dateToXmlGreogrianCalendar(Date date) {
		if (date == null)
			return null;
		
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);
		XMLGregorianCalendar xgc = null;
		try {
			xgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return xgc;
	}
	
	/**
	 * Holt das Date aus dem XMLGregorianCalendar
	 * @param c der XMLGregorianCalendar
	 * @return das Date
	 */
	public static Date xmlGreogrianCalendarToDate(XMLGregorianCalendar c) {
		return c.toGregorianCalendar().getTime();
	}
	
	
	public static Date stringToDate(String dateStr) {
		Date date = null;
		try {
			date = DATE_ONLY.parse(dateStr);
		} catch (ParseException e) {
		    e.printStackTrace();
		}
		
		return date;
	}
	
	public static ElementIdType erzElementIdType(String id, boolean isSingleUse, LocalDateTime date) {
		ElementIdType idt = new ElementIdType();
		idt.setId(id);
		idt.setIdIsSingleUse(isSingleUse);
		idt.setIdValidUntil(StringUtils.dateToXmlGreogrianCalendar(date));
		return idt;
	}
	
	public static String monat(int m) {
		switch (m) {
		case 1:
			return "Jänner";
		case 2: 
			return "Februar";
		case 3:
			return "März";
		case 4:
			return "April";
		case 5:
			return "Mai";
		case 6:
			return "Juni";
		case 7:
			return "Juli";
		case 8:
			return "August";
		case 9:
			return "September";
		case 10:
			return "Oktober";
		case 11:
			return "November";
		case 12:
			return "Dezember";
		default:
			return "";
			
		}
	}
	
	
	
	public static String filesize(Long size) {
		if (size == null)
			return "";
		
		if (size == 0)
			return "0 Bytes";
		
		if (size == 1)
			return ("1 Byte");

		if (size < 1000)
			return size + " Bytes";
		
		if (size < 1000000)
			return BYTE.format(size / 1000.0) + " kB";
		
		if (size < 1000000000)
			return BYTE.format(size / 1000000.0) + " MB";
		
		return BYTE.format(size / 1000000000.0) + " GB";
	}
	
	public static String zahlweise(int z) {
		switch (z) {
		case 1:
			return "jährlich";
		case 2: 
			return "halbjährlich";
		case 4:
			return "quartalsweise";
		case 12:
			return "monatlich";
		default:
			return "unbekannt";
		}
	}
	


	
	public static String country(String c, String nichtAber, boolean html) {
		String linebreak = "\n";
		if (html)
			linebreak = "<br>";
		
		if (c.equalsIgnoreCase("at") && !c.equalsIgnoreCase(nichtAber))
			return "Austria"+linebreak;
		if (c.equalsIgnoreCase("de") && !c.equalsIgnoreCase(nichtAber))
			return "Deutschland"+linebreak;
		if (c.equalsIgnoreCase("ch") && !c.equalsIgnoreCase(nichtAber))
			return "Schweiz"+linebreak;
		if (c.equalsIgnoreCase("it") && !c.equalsIgnoreCase(nichtAber))
			return "Italien"+linebreak;
		if (c.equalsIgnoreCase("en") && !c.equalsIgnoreCase(nichtAber))
			return "England"+linebreak;
		
		return "";
	}
	


	/**
	 * Gibt das Geschlecht als String zurueck.
	 * @param isWeiblich wahr wenn weiblich
	 * @return das Geschlecht als String
	 */
	public static String geschlecht(boolean isWeiblich) {
				
		if (isWeiblich)
			return "weiblich";
		
		return "männlich";
	}

	/**
	 * Formattiere Cent als Euro
	 * @param cent der Betrag in Cent
	 * @return der Betrag als Sting in Euro
	 */
	public static String formatAsEuro(long cent) {
		String s = WAEHRUNG.format(Math.round(cent / 100));
		return s;
	}


	/**
	 * Extrahiert den Teil des Strings nach dem letzten "/" und dem letzten "\".
	 * @param p der Pfad 
	 * @return den letzten Teil des Strings oder null
	 */
	public static String extractFilenameFromPath(String p) {
		if (p == null) 
			return null;

		String nachForwardSlash = extractLastSectionAfterForwardSlash(p);
		String nachBackwardSlash = extractLastSectionAfterBackslash(nachForwardSlash);
		return nachBackwardSlash;
	}
	
	/**
	 * Extrahiert den Teil des Strings nach dem letzten "/".
	 * @param p der Pfad 
	 * @return den letzten Teil des Strings oder null
	 */
	public static String extractLastSectionAfterForwardSlash(String p) {
		if (p == null) 
			return null;
		String[] splitted = p.split("/");
		int length = splitted.length;
		if (length > 0) {
			int index = length-1;
			return splitted[index];
		}
		
		return null;
	}
	
	/**
	 * Extrahiert den Teil des Strings nach dem letzten "/".
	 * @param p der Pfad 
	 * @return den letzten Teil des Strings oder null
	 */
	public static String extractLastSectionAfterBackslash(String p) {
		if (p == null) 
			return null;
		String[] splitted = p.split("\\\\");
		int length = splitted.length;
		if (length > 0) {
			int index = length-1;
			return splitted[index];
		}
		
		return null;
	}




}
