package at.vvo.omds3.exampleservice.fake;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import at.vvo.omds.types.omds2Types.v2_16.PERSONType;
import at.vvo.omds.types.omds2Types.v2_16.PaketUmfCdType;
import at.vvo.omds.types.omds2Types.v2_16.VERTRAG;

import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.OMDSPackageInfoType;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.RequestedOMDSPackage;
import at.vvo.omds3.exampleservice.config.MaklerID;
import at.vvo.omds3.exampleservice.config.User;

/**
 * Enthaelt das Modell fuer den User.
 * @author Jens Buehring
 *
 */
public class ModelForUser {
	
	private User user;
	private Map<String, ModelForMaklerID> contentForMaklerID;
	private List<RequestedOMDSPackage> listeDatensaetze;

	/**
	 * Constructor
	 * @param u der User, für den dieses Modell angelegt wurde
	 * @param personen die Liste aller Personen
	 * @param vertraege die Liste aller Vertraege
	 * @param minAnz die min Anzahl Vertraege pro Vermnr
	 * @param maxAnz die max Anzahl Vertrage pro Vermnr
	 */
	public ModelForUser(User u, Map<String, PERSONType> personen, Map<String, VERTRAG> vertraege, int minAnz, int maxAnz) {
		this.contentForMaklerID = new HashMap<>();
		listeDatensaetze = new ArrayList<>();
		this.user = u;
		fuegeVertraegeHinzu(personen, vertraege, minAnz, maxAnz);
	}

	public ModelForMaklerID getModel() {
		return contentForMaklerID.get(user.getMaklerID().getMaklerID());
	}

	public void erzeugeOMDSDatensaetze(Map<String, RequestedOMDSPackage> mapOMDSPacketById, PaketUmfCdType paketUmf) {
		
		// iteriere ueber alle MaklerIDs
		for (String maklerID : contentForMaklerID.keySet()) {
			ModelForMaklerID model = contentForMaklerID.get(maklerID);
			RequestedOMDSPackage o = model.erzeugeOMDSDatensatzUndInfo(mapOMDSPacketById, paketUmf);
			listeDatensaetze.add(o);
		}
	}

	public void fuegeVertraegeHinzu(Map<String, PERSONType> personen, Map<String, VERTRAG> vertraege, int minAnz, int maxAnz) {
		String maklerID = user.getMaklerID().getMaklerID();
		
		if (contentForMaklerID.containsKey(maklerID)) {
			
			// es gibt schon ein Model fuer die MaklerID, ergaenze das bestehende
			ModelForMaklerID m = contentForMaklerID.get(maklerID);
			m.fuegeVertraegeHinzu(personen, vertraege, minAnz, maxAnz);
		} else {
			// es gibt noch kein Model fuer die MaklerID, erzeuge es neu
			contentForMaklerID.put(maklerID, new ModelForMaklerID(user.getMaklerID(), personen, vertraege, minAnz, maxAnz));
		}
		
	}

	public List<OMDSPackageInfoType> getListeOMDSDatensaetze(MaklerID maklerID) {
		
		List<OMDSPackageInfoType> listeOhnePayload = new ArrayList<>();
		for (RequestedOMDSPackage o : listeDatensaetze) {
			listeOhnePayload.add(o.getOmdsPackage().getOmdsPackageInfo());
		}
		
		return listeOhnePayload;
	}

	public User getUser() {
		return user;
	}

	public Map<String, ModelForMaklerID> getContentForMaklerID() {
		return contentForMaklerID;
	}

	public List<RequestedOMDSPackage> getListeDatensaetze() {
		return listeDatensaetze;
	}

	public void entferneAlteOMDSDatensaetze(Map<String, RequestedOMDSPackage> mapOMDSDatensaetzeById, Date now) {
		
		List<RequestedOMDSPackage> neueListe = new ArrayList<>();
		for (RequestedOMDSPackage p : listeDatensaetze) {
			
			XMLGregorianCalendar cal = p.getOmdsPackage().getOmdsPackageInfo().getOmdsPackageIdDetails().getIdValidUntil();
			
			Date datensatzValidUntil = null;
			
			if (cal != null)
				datensatzValidUntil = StringUtils.xmlGreogrianCalendarToDate(cal);
			
			if (datensatzValidUntil != null && datensatzValidUntil.before(now)) {
				// der kann weg
				String id = p.getRequestedOmdsPackageId();
				mapOMDSDatensaetzeById.remove(id);
			} else {
				// der bleibt
				neueListe.add(p);
			}
		}
		
		this.listeDatensaetze = neueListe;
	}

	
}
