package at.vvo.omds3.exampleservice.fake.polDocuments;

import java.io.Serializable;
import java.util.List;

public class MCPolicyPartnerRoleModel implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String partnerNumber;
    protected List<MCPartnerRoleType> partnerRole;
    
	public String getPartnerNumber() {
		return partnerNumber;
	}
	public void setPartnerNumber(String partnerNumber) {
		this.partnerNumber = partnerNumber;
	}
	public List<MCPartnerRoleType> getPartnerRole() {
		return partnerRole;
	}
	public void setPartnerRole(List<MCPartnerRoleType> partnerRole) {
		this.partnerRole = partnerRole;
	}
    
    
    
    

}
