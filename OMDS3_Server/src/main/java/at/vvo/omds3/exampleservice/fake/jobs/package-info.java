/**
 * 
 */
/**
 * In diesem Package liegen Jobs, die im Hintergrund ausgeführt werden.
 * 
 * @author Jens Buehring
 *
 */
package at.vvo.omds3.exampleservice.fake.jobs;