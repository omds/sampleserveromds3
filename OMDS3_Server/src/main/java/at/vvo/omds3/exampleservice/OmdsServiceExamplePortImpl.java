package at.vvo.omds3.exampleservice;

import java.util.List;

import jakarta.annotation.Resource;
import jakarta.xml.ws.WebServiceContext;
import jakarta.xml.ws.soap.MTOM;

import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.GetClaimRequestType;
import at.vvo.omds.types.omds3Types.r1_11_0.service.ServiceFaultMsg;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.*;
import org.apache.cxf.security.SecurityContext;

import at.vvo.omds.types.omds3Types.r1_11_0.common.*;
import at.vvo.omds.types.omds3Types.r1_11_0.service.*;

import at.vvo.omds3.exampleservice.config.MaklerID;
import at.vvo.omds3.exampleservice.config.User;
import at.vvo.omds3.exampleservice.fake.VuModel;
import at.vvo.omds3.exampleservice.fake.configuration.WrapperUserForSoapTransport;
import at.vvo.omds3.exampleservice.fake.polDocuments.Polizzendokumente;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dies ist die Beispielimplementierung des OMDS-Webservice.
 */

@MTOM(enabled = true, threshold = 1024)
@jakarta.jws.WebService(
        serviceName = "omdsService",
        portName = "omdsServicePort",
        targetNamespace = "urn:omds3Services-1-4-0",
        wsdlLocation = "classpath:def/r1_11_0/omds3Services.wsdl",
        endpointInterface = "at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType")
                      
public class OmdsServiceExamplePortImpl extends OmdsServicePortImpl {

    private static final Logger LOG = LoggerFactory.getLogger(OmdsServiceExamplePortImpl.class);
    //private static final ObjectFactory ofTyp30 = new ObjectFactory();
    
    @Resource
    WebServiceContext wsContext;
    /**
     * Construktor
     */
    public OmdsServiceExamplePortImpl() {

    }
    
    @Override
    public at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.OMDSPackageListResponse getOMDSPackageList(
            at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.OMDSPackageListRequest parameters
    ) throws ServiceFaultMsg {

        LOG.info("Executing operation getOMDSPackageList");
        System.out.println(parameters);

        try {
        	String username = extractUsername(((org.apache.cxf.jaxws.context.WrappedMessageContext)wsContext.getMessageContext()).getWrappedMessage());
        	MaklerID maklerID = detMaklerID(parameters.getAgentFilter());
        	VuModel vu = VuModel.getInstance();
        	List<OMDSPackageInfoType> listeDatensaetze = vu.getListeOmdsDatensaetze(username, maklerID);
        	OMDSPackageListResponse response = new OMDSPackageListResponse();
        	response.getOmdsPackageInfo().addAll(listeDatensaetze);
//        	Packages packages = Packages.getInstance();
//            at.vvo.omds.types.omds30Types.OMDSPackageListResponse _return = packages.convertPackages();
            return response;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFault("serviceFault...");
    }


	@Override
    public ArcImageResponse getArcImage(ArcImageRequest parameters) throws ServiceFaultMsg {
        LOG.info("Executing operation getArcImage");
        System.out.println(parameters);
        try {
//        	String username = extractUsername(((org.apache.cxf.jaxws.context.WrappedMessageContext)wsContext.getMessageContext()).getWrappedMessage());
//        	MaklerID maklerID = detMaklerID(parameters.getAuthFilter());
        	Polizzendokumente policyDocuments = Polizzendokumente.getInstance();
        	ArcImageResponse _return = policyDocuments.getPolicyById(parameters.getArcImageId());          
            return _return;

        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFault("serviceFault...");
    }

    @Override
    public DeepLinkBusinessObjectResponse deepLinkClaim(
    		DeepLinkClaimRequest parameters 
    )  throws ServiceFaultMsg {
        LOG.info("Executing operation deepLinkClaim");
        System.out.println(parameters);
        try {
        	
        	String username = extractUsername(((org.apache.cxf.jaxws.context.WrappedMessageContext)wsContext.getMessageContext()).getWrappedMessage());
        	MaklerID maklerID = detMaklerID(parameters.getAuthFilter());
        	VuModel vu = VuModel.getInstance();
        	DeepLinkBusinessObjectResponse _return = vu.getLogin(username);

        	return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFault("serviceFault...");
    }


    
    @Override
    public OMDSPackageResponse getOMDSPackage(
    		OMDSPackageRequest parameters 
    ) throws ServiceFaultMsg  {
    	LOG.info("Executing operation getOMDSPackage");
    	System.out.println(parameters);
    	try {
    		String username = extractUsername(((org.apache.cxf.jaxws.context.WrappedMessageContext)wsContext.getMessageContext()).getWrappedMessage());
    		MaklerID maklerID = detMaklerID(parameters.getAgentFilter());
    		
    		VuModel vu = VuModel.getInstance();
    		List<RequestedOMDSPackage> listeAngefragteDatensaete = vu.getOMDSDatensaetzeById(username, maklerID, parameters.getVUNr(), parameters.getAgentFilter(), parameters.getOmdsPackageId());
    		
    		OMDSPackageResponse response = new OMDSPackageResponse();
    		response.getRequestedOmdsPackage().addAll(listeAngefragteDatensaete);
    		
//    		Packages packages = Packages.getInstance();
//    		at.vvo.omds.types.omds30Types.OMDSPackageResponse _return = packages.getInstance().getPackagebyPackageId(parameters.getOmdsPackageId().get(0));
    		return response;
    	} catch (java.lang.Exception ex) {
    		ex.printStackTrace();
    		throw new RuntimeException(ex);
    	}
    	//throw new ServiceFault("serviceFault...");
    }

    @Override
    public DeepLinkBusinessObjectResponse login(
    		LoginRequestType parameters 
    ) throws ServiceFaultMsg {
        LOG.info("Executing operation login");
        try {
        	
        	VuModel vu = VuModel.getInstance();
        	
        	String username = extractUsername(((org.apache.cxf.jaxws.context.WrappedMessageContext)wsContext.getMessageContext()).getWrappedMessage());
        	MaklerID maklerID = detMaklerID(parameters.getAuthFilter());
        	DeepLinkBusinessObjectResponse _return = vu.getLogin(username);
            
            return _return;
        	
        } catch (java.lang.Exception ex) {
        	LOG.atWarn().log(ex.getMessage());
            throw new ServiceFaultMsg("ServiceFault login "+ex.getMessage(),ex);
        }
    }

    @Override
    public DeepLinkBusinessObjectResponse deepLinkOffer(
    		DeepLinkOfferRequest parameters 
    )  throws ServiceFaultMsg {
        LOG.info("Executing operation deepLinkOffer");
        System.out.println(parameters);
       try {
       	VuModel vu = VuModel.getInstance();
    	
       	String username = extractUsername(((org.apache.cxf.jaxws.context.WrappedMessageContext)wsContext.getMessageContext()).getWrappedMessage());
       	MaklerID maklerID = detMaklerID(parameters.getAuthFilter());
       	DeepLinkBusinessObjectResponse _return = vu.getLogin(username);
           return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFault("serviceFault...");
    }

    @Override
    public DeepLinkBusinessObjectResponse deepLinkPartner(
    		DeepLinkPartnerRequest parameters 
    ) throws ServiceFaultMsg  {
        LOG.info("Executing operation deepLinkPartner");
        System.out.println(parameters);
        try {
        	VuModel vu = VuModel.getInstance();
        	
        	String username = extractUsername(((org.apache.cxf.jaxws.context.WrappedMessageContext)wsContext.getMessageContext()).getWrappedMessage());
        	MaklerID maklerID = detMaklerID(parameters.getAuthFilter());
        	DeepLinkBusinessObjectResponse _return = vu.getLogin(username);
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFault("serviceFault...");
    }

    @Override
    public DeepLinkBusinessObjectResponse deepLinkPolicy(
    		DeepLinkPolicyRequest parameters 
    ) throws ServiceFaultMsg  {
        LOG.info("Executing operation deepLinkPolicy");
        System.out.println(parameters);
       try {
       	VuModel vu = VuModel.getInstance();
    	
       	String username = extractUsername(((org.apache.cxf.jaxws.context.WrappedMessageContext)wsContext.getMessageContext()).getWrappedMessage());
       	MaklerID maklerID = detMaklerID(parameters.getAuthFilter());
       	DeepLinkBusinessObjectResponse _return = vu.getLogin(username);
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFault("serviceFault...");
    }

    @Override
    public ArcImageInfosResponse getArcImageInfos(
    		ArcImageInfosRequest parameters 
    ) throws ServiceFaultMsg  {
        LOG.info("Executing operation getArcImageInfos");
        System.out.println(parameters);
        try {
//        	String username = extractUsername(((org.apache.cxf.jaxws.context.WrappedMessageContext)wsContext.getMessageContext()).getWrappedMessage());
//        	MaklerID maklerID = detMaklerID(parameters.getAuthFilter());
//        	VuModel vu = VuModel.getInstance();
        	Polizzendokumente policyDocuments = Polizzendokumente.getInstance();
            ArcImageInfosResponse _return = policyDocuments.getInstance().getListeArcImageInfos(parameters);
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFault("serviceFault...");
    }

    @Override
    public UserDataResponse getUserData(
    		UserDataRequest parameters 
    ) throws ServiceFaultMsg  {
        LOG.info("Executing operation getUserData");
        System.out.println(parameters);

        try {
        	
        	String username = extractUsername(((org.apache.cxf.jaxws.context.WrappedMessageContext)wsContext.getMessageContext()).getWrappedMessage());

        	// get user from the model
        	User u = VuModel.getInstance().getUsersByUsername().get(username);
            UserDataResponse _return = WrapperUserForSoapTransport.initializeUser(u);
            
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFault("serviceFault...");
    }
    
    
    
    private static String extractUsername(org.apache.cxf.message.Message message) {
    	String username = (String) message.get("Username");
        LOG.info("Entered username: " + username);
    	SecurityContext sc = message.get(SecurityContext.class);
    	if (sc != null) {
    		username = sc.getUserPrincipal().getName();
    	}
    	return username;
    }
    
    
    /**
     * Bestimmt die MaklerID anhand des AgentFilter-Elements
     * @param agentFilter
     * @return
     */
	private MaklerID detMaklerID(AgentFilterType agentFilter) {
		if (agentFilter == null)
			return null;
		
		MaklerID maklerID = new MaklerID();
		maklerID.setMaklerID(agentFilter.getMaklerID());
		maklerID.setVermnr(agentFilter.getVermnr());
		return maklerID;
	}
    
	   /**
     * Bestimmt die MaklerID anhand des AgentFilter-Elements
     * @param authFilter
     * @return
     */
	private MaklerID detMaklerID(AuthorizationFilter authFilter) {
		if (authFilter == null)
			return null;
		
		if (!(authFilter instanceof AgentFilterType))
			return null;
	
		return detMaklerID((AgentFilterType) authFilter);
	}

    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#addDocToBusinessCase(at.vvo.omds.types.omds3Types.r1_11_0.on1basis.AddDocToBusinessCaseRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on1basis.AddDocToBusinessCaseResponseType addDocToBusinessCase(at.vvo.omds.types.omds3Types.r1_11_0.on1basis.AddDocToBusinessCaseRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation addDocToBusinessCase");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on1basis.AddDocToBusinessCaseResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }

    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#calculateSachPrivat(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CalculateSachPrivatRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CalculateSachPrivatResponseType calculateSachPrivat(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CalculateSachPrivatRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation calculateSachPrivat");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CalculateSachPrivatResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }



    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#createApplicationKfz(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CreateApplicationKfzRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CreateApplicationKfzResponseType createApplicationKfz(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CreateApplicationKfzRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation createApplicationKfz");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CreateApplicationKfzResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }

    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#createOfferKfz(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CreateOfferKfzRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CreateOfferKfzResponseType createOfferKfz(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CreateOfferKfzRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation createOfferKfz");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CreateOfferKfzResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }



    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#searchClaim(at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SearchClaimRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SearchClaimResponseType searchClaim(at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SearchClaimRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation searchClaim");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SearchClaimResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }

    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#getStateChanges(at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetStateChangesRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetStateChangesResponseType getStateChanges(at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetStateChangesRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation getStateChanges");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetStateChangesResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }



    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#submitClaim(at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SubmitClaimRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SubmitClaimResponseType submitClaim(at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SubmitClaimRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation submitClaim");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SubmitClaimResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }



    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#getNumberOfDocuments(at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetNumberOfDocumentsRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetNumberOfDocumentsResponseType getNumberOfDocuments(at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetNumberOfDocumentsRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation getNumberOfDocuments");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetNumberOfDocumentsResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }


    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#getDocumentsOfPeriod(at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetDocumentsOfPeriodRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetDocumentsOfPeriodResponseType getDocumentsOfPeriod(at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetDocumentsOfPeriodRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation getDocumentsOfPeriod");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetDocumentsOfPeriodResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }

    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#getDocumentsOfObject(at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetDocumentsOfObjectRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetDocumentsOfObjectResponseType getDocumentsOfObject(at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetDocumentsOfObjectRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation getDocumentsOfObject");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on1basis.GetDocumentsOfObjectResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }


    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#calculateKfz(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CalculateKfzRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CalculateKfzResponseType calculateKfz(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CalculateKfzRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation calculateKfz");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.CalculateKfzResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }


    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#createOfferSachPrivat(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CreateOfferSachPrivatRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CreateOfferSachPrivatResponseType createOfferSachPrivat(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CreateOfferSachPrivatRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation createOfferSachPrivat");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CreateOfferSachPrivatResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }


    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#createApplicationSachPrivat(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CreateApplicationSachPrivatRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CreateApplicationSachPrivatResponseType createApplicationSachPrivat(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CreateApplicationSachPrivatRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation createApplicationSachPrivat");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.CreateApplicationSachPrivatResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }


    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#getClaim(at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.SpezifikationSchadenType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.GetClaimResponseType getClaim(GetClaimRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation getClaim");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.GetClaimResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }


    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#submitApplicationKfz(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.SubmitApplicationKfzRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.SubmitApplicationKfzResponseType submitApplicationKfz(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.SubmitApplicationKfzRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation submitApplicationKfz");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.SubmitApplicationKfzResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }

    /* (non-Javadoc)
     * @see at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType#submitApplicationSachPrivat(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.SubmitApplicationSachPrivatRequestType parameters)*
     */
    public at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.SubmitApplicationSachPrivatResponseType submitApplicationSachPrivat(at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.SubmitApplicationSachPrivatRequestType parameters) throws ServiceFaultMsg   {
        LOG.info("Executing operation submitApplicationSachPrivat");
        System.out.println(parameters);
        try {
            at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.SubmitApplicationSachPrivatResponseType _return = null;
            return _return;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ServiceFaultMsg("ServiceFaultMsg...");
    }

}
