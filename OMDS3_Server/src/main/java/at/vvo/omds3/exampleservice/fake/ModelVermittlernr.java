package at.vvo.omds3.exampleservice.fake;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;


import at.vvo.omds.types.omds2Types.v2_16.PERSONType;
import at.vvo.omds.types.omds2Types.v2_16.VERTRAG;
import at.vvo.omds3.exampleservice.fake.omdsPackage.CreatorVertrag;

/**
 * Enthaelt den Bestands-Content fuer eine Vermittlernummer.
 *  
 * @author Jens
 *
 */
public class ModelVermittlernr {

	String maklerID;
	String vermnr;
	List<VERTRAG> listeVertraegeDieserVermittlernr;
//	Map<String, SCHADENType> mapSchaeden;

	/**
	 * Erzeugt ein Modell fuer die angegebene Vermittlernummer.
	 * @param maklerID die MaklerID
	 * @param vermnr die Vermittlernr
	 */
	public ModelVermittlernr(String maklerID, String vermnr, Map<String, PERSONType> personen, Map<String, VERTRAG> vertraege, int minAnz, int maxAnz) {
		this.maklerID = maklerID;
		this.vermnr = vermnr;
	
		listeVertraegeDieserVermittlernr = new ArrayList<>();
//		mapSchaeden = new HashMap<>();
		
		fuegeVertraegeHinzu(personen, vertraege, minAnz, maxAnz);
		
	}

	public String getMaklerID() {
		return maklerID;
	}

	public String getVermnr() {
		return vermnr;
	}

	public List<VERTRAG> getListeVertraegeDieserVermittlernr() {
		return listeVertraegeDieserVermittlernr;
	}



	public void fuegeVertraegeHinzu(Map<String, PERSONType> personen, Map<String, VERTRAG> vertraege, int minAnz, int maxAnz) {
		Random randomGenerator = new Random();
		int anzahl = randomGenerator.nextInt(maxAnz - minAnz) + minAnz;
		List<VERTRAG> neueVertraegeFuerDieseVermnr = CreatorVertrag.erzeugeListeVertraege(anzahl, maklerID, vermnr, personen, vertraege);
		
		listeVertraegeDieserVermittlernr.addAll(neueVertraegeFuerDieseVermnr);
		
		// registiere die neuen Vertraege
		for (VERTRAG v : neueVertraegeFuerDieseVermnr) {
			vertraege.put(v.getPolizzennr(), v);
		}
	}

	

}
