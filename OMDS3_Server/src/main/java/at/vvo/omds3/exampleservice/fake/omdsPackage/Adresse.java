package at.vvo.omds3.exampleservice.fake.omdsPackage;

/**
 * Eine Adresse
 */
public class Adresse {
	
	String hausnr;
	String landesCd;
	long pac;
	String PLZ;
	String ort;
	String strasse;
	String zusatz;
	
	public String getHausnr() {
		return hausnr;
	}
	public void setHausnr(String hausnr) {
		this.hausnr = hausnr;
	}
	public String getLandesCd() {
		return landesCd;
	}
	public void setLandesCd(String landesCd) {
		this.landesCd = landesCd;
	}
	public long getPac() {
		return pac;
	}
	public void setPac(long pac) {
		this.pac = pac;
	}
	public String getStrasse() {
		return strasse;
	}
	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}
	public String getZusatz() {
		return zusatz;
	}
	public void setZusatz(String zusatz) {
		this.zusatz = zusatz;
	}
	public String getPLZ() {
		return PLZ;
	}
	public void setPLZ(String pLZ) {
		PLZ = pLZ;
	}
	public String getOrt() {
		return ort;
	}
	public void setOrt(String ort) {
		this.ort = ort;
	}
	
	
}
