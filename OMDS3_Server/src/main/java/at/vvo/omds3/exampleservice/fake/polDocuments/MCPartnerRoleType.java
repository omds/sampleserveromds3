package at.vvo.omds3.exampleservice.fake.polDocuments;

public enum MCPartnerRoleType {
	
    /**
     * Versicherungsnehmer
     * 
     */
    VN,

    /**
     * Versicherungsnehmer
     * 
     */
    VP;
	
    public String value() {
        return name();
    }

    public static MCPartnerRoleType fromValue(String v) {
        return valueOf(v);
    }

}
