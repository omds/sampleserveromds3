package at.vvo.omds3.exampleservice.fake.jobs;

import static org.quartz.CronScheduleBuilder.dailyAtHourAndMinute;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import at.vvo.omds3.exampleservice.config.AppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModelScheduler {
	
	private static final String GROUP1 = "group1";
	private static final Logger LOG = LoggerFactory.getLogger(ModelScheduler.class);
   
    /** starte den Scheduler */
    public static void start() {
    	
    	LOG.atDebug().log("Start Job Scheduler");

    	try {
    		// Grab the Scheduler instance from the Factory
    		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

    		// and start it off
    		scheduler.start();

    		// define the job and tie it to our MyJob class
    		JobDetail createNewPoliciesJob = newJob(CreateAddPoliciesJob.class)
    				.withIdentity("createNewPoliciesJob", GROUP1)
    				.build();

    		JobDetail cleanModelJob = newJob(CleanModelJob.class)
    				.withIdentity("cleanModelJob", GROUP1)
    				.build();

    		
    		// Trigger the job to run now, and then repeat every 40 seconds
    		Trigger trigger1 = newTrigger()
    				.withIdentity("trigger1", GROUP1)
    				.startNow()
    				.withSchedule(simpleSchedule()
    						.withIntervalInSeconds(AppContext.getAppConfig().getIntervalForModelupdateInSeconds())
    						.repeatForever())
    				.build();
    		
    		Trigger trigger2 = newTrigger()
    				.withIdentity("trigger2", GROUP1)
//    				.withSchedule(dailyAtHourAndMinute(17, 30))

    				.withSchedule(dailyAtHourAndMinute(
    						AppContext.getAppConfig().getHourOfDayForModelClean(), 
    						AppContext.getAppConfig().getMinuteOfHourForModelClean()))
    			    .build();


    		// Tell quartz to schedule the job using our trigger
    		scheduler.scheduleJob(createNewPoliciesJob, trigger1);
    		scheduler.scheduleJob(cleanModelJob, trigger2);
    		
    		
    	} catch (SchedulerException e) {
    		LOG.atError().log("Scheduler konnte nicht gestartet werden.");
    		e.printStackTrace();
    	}
    }
    
}
