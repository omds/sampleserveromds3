package at.vvo.omds3.exampleservice.fake.polDocuments;

import java.io.Serializable;
import java.util.List;

import at.vvo.omds.types.omds3Types.r1_11_0.common.ServiceFault;

public class MCArcImageInfosResponse implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected List<MCArcImageInfo> arcImageInfo;
    protected ServiceFault serviceFault;
    
	public List<MCArcImageInfo> getArcImageInfo() {
		return arcImageInfo;
	}
	public void setArcImageInfo(List<MCArcImageInfo> arcImageInfo) {
		this.arcImageInfo = arcImageInfo;
	}
	public ServiceFault getServiceFault() {
		return serviceFault;
	}
	public void setServiceFault(ServiceFault serviceFault) {
		this.serviceFault = serviceFault;
	}
    
    

}
