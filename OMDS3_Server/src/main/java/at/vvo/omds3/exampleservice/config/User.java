package at.vvo.omds3.exampleservice.config;

import java.util.List;

/**
 * User ist ein Makler, der auf den Server zugreift.
 * Dem User ist eine MaklerID mit einer oder mehreren Vermittlernummern zugewiesen.
 * 
 * @author Jens Buehring
 *
 */
public class User {

	String vorname;
	String familienname;
	String geschlecht;
	
	
	String username;
	String passwort;
	
	String hausnr;
	String landesCd;
	String ort;
	long pac;
	String personennr;
	String plz;
	String strasse;
	String zusatz;
	
	List<String> services;
	
	MaklerID maklerID;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String user) {
		this.username = user;
	}
	public String getPasswort() {
		return passwort;
	}
	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}
	public String getHausnr() {
		return hausnr;
	}
	public void setHausnr(String hausnr) {
		this.hausnr = hausnr;
	}
	public String getLandesCd() {
		return landesCd;
	}
	public void setLandesCd(String landesCd) {
		this.landesCd = landesCd;
	}
	public String getOrt() {
		return ort;
	}
	public void setOrt(String ort) {
		this.ort = ort;
	}
	public long getPac() {
		return pac;
	}
	public void setPac(long pac) {
		this.pac = pac;
	}
	public String getPersonennr() {
		return personennr;
	}
	public void setPersonennr(String personennr) {
		this.personennr = personennr;
	}
	public String getPlz() {
		return plz;
	}
	public void setPlz(String plz) {
		this.plz = plz;
	}
	public String getStrasse() {
		return strasse;
	}
	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}
	public String getZusatz() {
		return zusatz;
	}
	public void setZusatz(String zusatz) {
		this.zusatz = zusatz;
	}
	public List<String> getServices() {
		return services;
	}
	public void setServices(List<String> services) {
		this.services = services;
	}
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getFamilienname() {
		return familienname;
	}
	public void setFamilienname(String familienname) {
		this.familienname = familienname;
	}
	public String getGeschlecht() {
		return geschlecht;
	}
	public void setGeschlecht(String geschlecht) {
		this.geschlecht = geschlecht;
	}
	public MaklerID getMaklerID() {
		return maklerID;
	}
	public void setMaklerID(MaklerID maklerID) {
		this.maklerID = maklerID;
	}

	
}
