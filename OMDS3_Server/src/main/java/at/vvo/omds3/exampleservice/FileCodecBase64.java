package at.vvo.omds3.exampleservice;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.codec.binary.Base64;

public class FileCodecBase64 {
	  
    /**
     * This method converts the content of a source file into Base64 encoded data and saves that to a target file.
     * If isChunked parameter is set to true, there is a hard wrap of the output  encoded text.
     * @throws Exception 
     */
    public static byte[] encode(String sourceFile) throws Exception {
 
        byte[] base64EncodedData = Base64.encodeBase64(loadFileAsBytesArray(sourceFile));
 
        return base64EncodedData;
    }
    
    public static byte[] decode(byte[] arr) {
    	return Base64.decodeBase64(arr);
    }
    
    /**
     * This method loads a file from file system and returns the byte array of the content.
     *
     * @param fileName
     * @return
     * @throws IOException 
     * 
     */
    public static byte[] loadFileAsBytesArray(String fileName) throws IOException {
 
        File file = new File(fileName);
        int length = (int) file.length();
        BufferedInputStream reader = new BufferedInputStream(new FileInputStream(file));
        byte[] bytes = new byte[length];
        reader.read(bytes, 0, length);
        reader.close();
        return bytes;
 
    }
 
    /**
     * This method writes byte array content into a file.
     *
     * @param fileName
     * @param content
     * @throws IOException
     */
    public static void writeByteArraysToFile(String fileName, byte[] content) throws IOException {
 
        File file = new File(fileName);
        BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
        writer.write(content);
        writer.flush();
        writer.close();
 
    }
    
    public static byte[] getInputStreamAsByteArray(InputStream is) throws IOException {
	    ByteArrayOutputStream buffer = new ByteArrayOutputStream();

	    int len;
	    byte[] data = new byte[100000]; // adapt buffer size to your needs
	    while ((len = is.read(data, 0, data.length)) != -1) {
	        buffer.write(data, 0, len);
	    }

	    buffer.flush();
	    return buffer.toByteArray();
	}
    
}
