package at.vvo.omds3.exampleservice.config;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Application-Context
 * 
 * @author Jens Buehring
 *
 */

public class AppContext {
	
	private static ClassPathXmlApplicationContext context;
	
	public static ClassPathXmlApplicationContext getInstance() {
		if (context == null)
			context = new ClassPathXmlApplicationContext("spring-configuration.xml");
		return context;
	}
	
	public static BackendConfigBean getAppConfig() {
		return AppContext.getInstance().getBean("backendConfig", BackendConfigBean.class);
	}
	
	
}
