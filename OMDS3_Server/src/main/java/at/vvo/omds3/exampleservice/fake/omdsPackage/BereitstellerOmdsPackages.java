package at.vvo.omds3.exampleservice.fake.omdsPackage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import at.vvo.omds.types.omds3Types.r1_11_0.common.ElementIdType;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.OMDSPackageInfoType;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.OMDSPackageListResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.OMDSPackageResponse;
import at.vvo.omds3.exampleservice.fake.StringUtils;

public class BereitstellerOmdsPackages {


	private static List<OMDSPackageInfoType> createListePackageInfos(List<OmdsPackage> rohePackages) {
		List<OMDSPackageInfoType> l = new ArrayList<>();

		for (Iterator<OmdsPackage> iterator = rohePackages.iterator(); iterator.hasNext();) {
			OmdsPackage p = (OmdsPackage) iterator.next();

			OMDSPackageInfoType neuesPackage = new OMDSPackageInfoType();
			neuesPackage.setVUNr(p.getVuNr());
//			neuesPackage.setAgentID(p.getAgentID());
			neuesPackage.setOmdsPackageIdDetails(erzElementIdType(p.getOmdsPackageIdDetails()));
			
			
			neuesPackage.setTimeStamp(StringUtils.dateToXmlGreogrianCalendar(p.getTimestamp()));
			neuesPackage.setPackageSize(p.getPackageSize());
			neuesPackage.setPackageExtent(p.getPackageExtent());
			neuesPackage.getPackageContentCode().addAll(p.getPackageContentCodes());
			l.add(neuesPackage);
		}

		return l;
	}
	
	private static ElementIdType erzElementIdType(OmdsPackageIdDetails d) {
		return StringUtils.erzElementIdType(d.getId(), d.isSingleUse(), d.idValidUntil);
	}
	
	public static List<OMDSPackageInfoType> getListeOmdsPackages(List<OmdsPackage> listeOmdsPackages) {
		return createListePackageInfos(listeOmdsPackages);
	}

	public static OMDSPackageListResponse getPackageListResponse(List<OmdsPackage> listeOmdsPackages) {
		OMDSPackageListResponseImpl resp = new OMDSPackageListResponseImpl();
		resp.setOmdsPackageInfo(getListeOmdsPackages(listeOmdsPackages));
		return resp;
	}
	
	public static OMDSPackageResponse getSingleListResponse(OmdsPackage singlePackage) {
		OMDSPackageResponse resp = new OMDSPackageResponse();
		
		return resp;
	}
}
