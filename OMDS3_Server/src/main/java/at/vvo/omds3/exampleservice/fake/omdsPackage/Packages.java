package at.vvo.omds3.exampleservice.fake.omdsPackage;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import at.vvo.omds.types.omds3Types.r1_11_0.common.ElementIdType;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.OMDSPackageInfoType;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.OMDSPackageListResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.OMDSPackageResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.RequestedOMDSPackage;
import at.vvo.omds3.exampleservice.FileCodecBase64;
import at.vvo.omds3.exampleservice.fake.StringUtils;

import jakarta.activation.DataHandler;


public class Packages {
	
	private static Packages p;
	private Map<String, OmdsPackage> mapPackages;
	private List<OmdsPackage> listPackages;
	
	private static final DecimalFormat VUNR_FORMAT = new DecimalFormat("0000");

	/**
	 * Privater Constructor
	 */
	private Packages() {
		initializePackages();
	}
	
	/**
	 * Erzeugt eine Instance von Connections und gibt diese zurueck.
	 * @return die Instance
	 */
	public static Packages getInstance() {
		if (p == null) {
			p = new Packages();
		}
		return p;
	}
	

	private void initializePackages() {
		mapPackages = new HashMap<>();
		listPackages = ProduceFakeOmdsPackageInfos.fakeOmdsPackageInfoListe();
		for(OmdsPackage c : listPackages) {
			mapPackages.put(c.getOmdsPackageIdDetails().getId(), c);
		}
	}
	
	public OMDSPackageListResponse convertPackages() {
		return BereitstellerOmdsPackages.getPackageListResponse(listPackages);
	}
	
	
	/* Return all Omds Packages*/
	public Map<String, OmdsPackage> getMapOfPackages() {
		return mapPackages;
	}
	
	
	/* Get Single Omds Package*/
	public OMDSPackageResponse getPackagebyPackageId(String packageId) throws IOException {
	
	if (getInstance().getMapOfPackages() == null)
		return null;
	return converSinglePackage(getInstance().getMapOfPackages().get(packageId));
	
	}
	
	public OMDSPackageResponse converSinglePackage(OmdsPackage p) throws IOException {
		
		OMDSPackageResponse resp = new OMDSPackageResponse();
		
		RequestedOMDSPackage model = new RequestedOMDSPackage();
		RequestedOMDSPackage.OmdsPackage packageModel = new RequestedOMDSPackage.OmdsPackage();
		
		OMDSPackageInfoType packageInfo = new OMDSPackageInfoType();
		packageInfo.setVUNr(p.getVuNr());
		packageInfo.setMaklerID(p.getAgentID());
		packageInfo.setOmdsPackageIdDetails(erzElementIdType(p.getOmdsPackageIdDetails()));
		packageInfo.setTimeStamp(StringUtils.dateToXmlGreogrianCalendar(p.getTimestamp()));
		packageInfo.setPackageSize(p.getPackageSize());
		packageInfo.setPackageExtent(p.getPackageExtent());
		packageInfo.getPackageContentCode().addAll(p.getPackageContentCodes());
		
		packageModel.setOmdsPackageInfo(packageInfo);
		
		// Encoding PDF document
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("fileSample"+packageInfo.getVUNr()+".zip");
		if(in != null) {
			DataHandler dh = new DataHandler(FileCodecBase64.getInputStreamAsByteArray(in), "application/pdf");
			packageModel.setContent(dh);
		}
		
		model.setOmdsPackage(packageModel);
		resp.getRequestedOmdsPackage().add(model);
		
		return resp;
	}
	
	private static ElementIdType erzElementIdType(OmdsPackageIdDetails d) {
		return StringUtils.erzElementIdType(d.getId(), d.isSingleUse(), d.idValidUntil);
	}

}
