package at.vvo.omds3.exampleservice.fake.polDocuments;

import java.util.Iterator;
import java.util.List;


import at.vvo.omds.types.omds3Types.r1_11_0.common.ElementIdType;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.ArcImageInfo;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.ArcImageInfosResponse;
import at.vvo.omds3.exampleservice.fake.StringUtils;

public class KonvertierePolizzenDokumente {
	
	public static ArcImageInfosResponse konvertiere(List<MCArcContent> policiesList) {
		
		ArcImageInfosResponse res = new ArcImageInfosResponse();

		for (Iterator<MCArcContent> iterator = policiesList.iterator(); iterator.hasNext();) {
			
			MCArcContent p = (MCArcContent) iterator.next();

			ArcImageInfo arcIm = new ArcImageInfo();
			arcIm.setArcContentLength(p.getArcImageInfo().getArcContentLength());
			arcIm.setArcContentType(p.getArcImageInfo().getArcContentType());
			arcIm.setDate(StringUtils.dateToXmlGreogrianCalendar(p.getArcImageInfo().getDate()));
			arcIm.setDocumentType(p.getArcImageInfo().getDocumentType());
			arcIm.setName(p.getArcImageInfo().getName());
			arcIm.setArcImageIdDetails(convertType(p));
			
			res.getArcImageInfo().add(arcIm);
		}
		
		return res;
	}
	
	private static ElementIdType convertType(MCArcContent p) {
		
		ElementIdType type = new ElementIdType();
		type.setId(p.getArcImageInfo().getArcImageIdDetails().getId());
		type.setIdIsSingleUse(p.getArcImageInfo().getArcImageIdDetails().isIdIsSingleUse());
		type.setIdValidUntil(null);
		
		return type;
	}

}
