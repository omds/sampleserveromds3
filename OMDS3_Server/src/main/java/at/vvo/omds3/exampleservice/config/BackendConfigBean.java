package at.vvo.omds3.exampleservice.config;

import java.util.List;

public interface BackendConfigBean {
	

	/**
	 * Gibt die APP-Id zurueck.
	 * @return die APP-Id als String
	 */
	public String getAppid();

	/**
	 * Gibt die VUNr zurueck.
	 * @return die VUNr
	 */
	public int getVunr();
	
	public List<User> getListUsers();

	/**
	 * Gibt die DVRNr zurueck.
	 * @return die Nummer
	 */
	public String getDvrNr();

	/**
	 * Gibt die OMDS Version zurueck.
	 * @return die OMDS Version als String
	 */
	public String getOmdsVersion();

	public int getMinAnzVertraegeInit();

	public int getMaxAnzVertraegeInit();

	public int getMinPersnnr();

	public int getMaxPersnnr();

	public int getMinAlter();

	public int getMaxAlter();

	public int getMaxZusatz();

	public int getMaxHausnr();

	public int getMinPolizzennr();

	public int getMaxPolizzennr();

	public int getMinAnzVertraegeErg();

	public int getMaxAnzVertraegeErg();

	public int getMinPackageNr();

	public int getMaxPackageNr();

	public int getIntervalForModelupdateInSeconds();

	public int getHourOfDayForModelClean();

	public int getMinuteOfHourForModelClean();
	
	public int getMinutesOMDSDatensatzValid();

}

