package at.vvo.omds3.exampleservice.fake.omdsPackage;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Transportobjekt Server-Client fuer die Informationen zu einer Id eines OMDS-Packages.
 * @author Jens Buehring
 *
 */
public class OmdsPackageIdDetails implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String id;
	LocalDateTime idValidUntil;
	boolean singleUse;
	
	/** Default Constructor */
	public OmdsPackageIdDetails() {
		super();
	}
	
	
	
	/**
	 * Constructor
	 * @param id
	 * @param idValidUntil
	 * @param singleUse
	 */
	public OmdsPackageIdDetails(String id, LocalDateTime idValidUntil, boolean singleUse) {
		super();
		this.id = id;
		this.idValidUntil = idValidUntil;
		this.singleUse = singleUse;
	}




	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public LocalDateTime getIdValidUntil() {
		return idValidUntil;
	}
	public void setIdValidUntil(LocalDateTime idValidUntil) {
		this.idValidUntil = idValidUntil;
	}
	public boolean isSingleUse() {
		return singleUse;
	}
	public void setSingleUse(boolean singleUse) {
		this.singleUse = singleUse;
	}

}
