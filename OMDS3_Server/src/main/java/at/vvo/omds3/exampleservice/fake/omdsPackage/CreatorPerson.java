package at.vvo.omds3.exampleservice.fake.omdsPackage;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import at.vvo.omds.types.omds2Types.v2_16.NATUERLICHEPERSONType;
import at.vvo.omds.types.omds2Types.v2_16.PERSONType;
import at.vvo.omds.types.omds2Types.v2_16.PersArtCdType;
import at.vvo.omds3.exampleservice.config.AppContext;
import at.vvo.omds3.exampleservice.fake.StringUtils;

public class CreatorPerson {
	
	
	private static final String[] NACHNAMEN = new String[] {"Gruber", "Winkler", "Weber", "Huber", "Meyer", "Schneider", 
			"Bauer", "Wimmer", "Wallner", "Wolf", "Steiner", "Pichler", "Moser", "Wagner", "Müller", "Hofer", "Maurer", "Leitner", 
			"Berger", "Fuchs", "Eder", "Fischer", "Schmid", "Weiss", "Wieser", "Schwarz", "Maier", "Reiter", "Winter", "Mayr", 
			"Schmidt", "Egger", "Baumgartner", "Lang", "Brunner", "Auer", "Unger", "Lechner", "Binder", "Aigner", "Weiß",
			"Ebner", "Koller", "Zimmermann", "Wiesinger", "Lehner", "Thaler", "Haas", "Zauner", "Schuster", "Walter", "Holzer",
			"Graf",	"Haider","Lackner",	"Strasser",	"Koch",	"Zach",	"Wurm",	"Stadler","König",
			"Böhm", "Krenn","Kaiser","Seidl","Kern","Fink",	"Kaufmann",	"Mair",	"Ortner","Hauser","Hofbauer","Riegler","Hackl",
			"Fritz","Posch", "Schober","Mayrhofer","Hofmann","Karner","Riedl","Traxler","Resch","Reisinger","Strobl","Kogler",
			"Ziegler","Zechner","Rainer","Vogl", "Weinberger","Neubauer","Schwaiger","Windisch","Thurner","Jäger","Brandstätter",
			"Horvath","Trummer","Grabner","Unterberger","Müllner","Friedl","Klein",	"Frank"}; 
	private static final String[] VORNAMEN_WEIBLICH = new String[] {"Monika", "Gabriella", "Ermine", "Anna",
			"Sophie", "Maria", "Emilia", "Elena", "Emma", "Lena", "Sarah", "Mia", "Laura", "Julia", "Andrea", "Doris",
			"Johanna",	"Lea", "Valentina","Leonie","Luisa", "Katharina","Amelie",	"Magdalena","Lara",	"Isabella",	"Theresa","Viktoria","Lisa","Lina",	"Marlene",
			"Annika","Elisa","Nina","Nora","Alina","Ella","Caroline","Maja","Franziska","Paula","Lilly","Valerie","Alice","Zoe","Eva",
			"Paulina","Olivia",	"Vanessa","Melissa","Pia","Matilda","Anja",	"Anna-Lena","Elisabeth","Christina","Antonia","Linda","Rosa",	"Ida"};
	private static final String[] VORNAMEN_MAENNLICH = new String[] {"Robert", "Alexander", "Michael",
			"Lukas", "David", "Jakob", "Elias", "Maximilian", "Alexander", "Jonas", "Paul", "Tobias", "Leon", "Felix", "Simon", "Raphael",
			"Niklas", "Philipp", "Julian", "Sebastian", "Fabian", "Heinz", "Matthias", "Moritz", "Matteo", "Florian", "Johannes", "Noah","Lorenz",
			"Luis", "August", "Michael", "Daniel", "Samuel", "Benjamin", "Walter", "Georg", "Gregor", "Nico", "Markus", "Valentin", 
			"Dominik", "Leo", "Josef", "Emil", "Leonhard", "Franz", "Andreas", "Gabriel", "Konstantin", "Anton", "Stefan", "Jan",
			"Manuel", "Adrian", "Ben", "Marcel", "Jonathan", "Vincent", "Theodor", "Finn", "Thomas", "Oliver", "Kilian", "Till", "Max", "Oskar",
			"Otto", "Reinhard", "Peter", "Christian", "Eugen", "Christoph", "Erik"};

	
	public static PERSONType erzeugePerson(Map<String, PERSONType> bereitsBestehendePersonen) {
		String personennr = erzeugePersonenNummer(bereitsBestehendePersonen.keySet());
		
		PERSONType person = new PERSONType();
		person.setPersonennr(personennr);
		person.setPersArtCd(PersArtCdType.N); // natuerliche Person
		person.setNATUERLICHEPERSON(erzeugeNatPerson());
		applyAdresse(person, AdressErzeuger.getInstance().erzeugeNeueAdresse());
		return person;
	}

	
	private static void applyAdresse(PERSONType person, Adresse adresse) {
		person.setHausnr(adresse.getHausnr());
		person.setLandesCd(adresse.getLandesCd());
		person.setOrt(adresse.getOrt());
		person.setPac(adresse.getPac());
		person.setPLZ(adresse.getPLZ());
		person.setStrasse(adresse.getStrasse());
		person.setZusatz(adresse.getZusatz());
	}	


	private static NATUERLICHEPERSONType erzeugeNatPerson() {
		NATUERLICHEPERSONType np = new NATUERLICHEPERSONType();
		np.setFamilienname(RandomUtils.randomOfStringArray(NACHNAMEN));
//		np.setFamilienstandCd(value);
		int minAlter = AppContext.getAppConfig().getMinAlter();
		int maxAlter = AppContext.getAppConfig().getMaxAlter();
		np.setGebdat(StringUtils.dateToXmlGreogrianCalendar(erzGeburtsdatum(minAlter, maxAlter)));
		
		boolean weiblich = RandomUtils.nextBoolean();
		if (weiblich) {
			np.setGeschlechtCd("2");
			np.setVorname(RandomUtils.randomOfStringArray(VORNAMEN_WEIBLICH));
		} else {
			np.setGeschlechtCd("1");
			np.setVorname(RandomUtils.randomOfStringArray(VORNAMEN_MAENNLICH));
		}
		return np;
	}

	private static Date erzGeburtsdatum(int minAlter, int maxAlter) {
		GregorianCalendar now = new GregorianCalendar();
		int currentYear = now.get(Calendar.YEAR);
		int firstYear = currentYear - maxAlter;
		int lastYear = currentYear - minAlter;
		Random randomGenerator = new Random();
		int year = randomGenerator.nextInt(lastYear - firstYear) + firstYear;
		int month = randomGenerator.nextInt(12);
		int dayOfMonth = randomGenerator.nextInt(31) + 1;
		
		GregorianCalendar birth = new GregorianCalendar(year, month, dayOfMonth);
		return birth.getTime();
	}

	/**
	 * Erzeugt eine neue Personennummer, die im Modell noch nicht vergeben ist.
	 * @param bestehendePersonennummern das Set der schon vergebenen Personennummern
	 * @return die neue Nummer
	 */
	private static String erzeugePersonenNummer(Set<String> bestehendePersonennummern) {
		int min = AppContext.getAppConfig().getMinPersnnr();
		int max = AppContext.getAppConfig().getMaxPersnnr();
		return RandomUtils.createNr(min, max, bestehendePersonennummern);
	}
}
