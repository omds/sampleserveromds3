package at.vvo.omds3.exampleservice.fake.omdsPackage;

import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

import at.vvo.omds3.exampleservice.config.AppContext;

public class AdressErzeuger {

	private static final String[] STRASSEN = new String[] {"Abraham-a-Sancta-Clara-Gasse",
			"Akademiestraße", "Albertinaplatz", "Alte Walfischgasse", "Am Gestade", "Am Hof", "An der Hülben", "Annagasse",
			"Auerspergstraße", "Augustinerbastei", "Augustinerstraße", "Auwinkel", "Babenbergerstraße", "Bäckerstraße",
			"Ballgasse", "Ballhausplatz", "Bankgasse", "Barbaragasse", "Bartensteingasse"
	
	};
	
	private static TreeMap<Integer, String> orte = new TreeMap<>();
	static {
		// befuelle Orte
		orte.put(1010, "Wien");
		orte.put(1020, "Wien");
		orte.put(1030, "Wien");
		orte.put(1040, "Wien");
		orte.put(1050, "Wien");
		orte.put(1060, "Wien");
		orte.put(1070, "Wien");
		orte.put(1080, "Wien");
		orte.put(1090, "Wien");
		orte.put(1100, "Wien");
		orte.put(1110, "Wien");
		orte.put(1120, "Wien");
		orte.put(1130, "Wien");
		orte.put(1140, "Wien");
		orte.put(1150, "Wien");
		orte.put(1160, "Wien");
		orte.put(1170, "Wien");
		orte.put(1180, "Wien");
		orte.put(1190, "Wien");
		orte.put(1200, "Wien");
		orte.put(1210, "Wien");
		orte.put(1220, "Wien");
		orte.put(1230, "Wien");
		orte.put(2340, "Mödling");
		
	}
	
	private static AdressErzeuger instance;
	
	private AdressErzeuger() {
		
	}

	public static AdressErzeuger getInstance() {
		if (instance == null)
			instance = new AdressErzeuger();
		return instance;
	}
	
	public Adresse erzeugeNeueAdresse() {
		Adresse a = new Adresse();
		String[] orte = bestimmeOrt();
		a.setOrt(orte[1]);
		a.setPLZ(orte[0]);
		a.setStrasse(bestimmeStrasse());
		a.setHausnr(bestimmeHausnummer());
		a.setZusatz(bestimmeZusatz());
		return a;
	}
	
	private String bestimmeZusatz() {
		if (RandomUtils.nextBoolean())
			return null;
		else {
			Random randomGenerator = new Random();
			Integer top = randomGenerator.nextInt(AppContext.getAppConfig().getMaxZusatz() - 1) + 1;
			return top.toString();
		}
	}

	private String bestimmeHausnummer() {
		Random randomGenerator = new Random();
		Integer hausnr = randomGenerator.nextInt(AppContext.getAppConfig().getMaxHausnr() - 1) + 1;
		return hausnr.toString();
	}

	private String bestimmeStrasse() {
		return RandomUtils.randomOfStringArray(STRASSEN);
	}
	
	
	
	private String[] bestimmeOrt() {
		int anz = orte.size();
		Random randomGenerator = new Random();
		int n = randomGenerator.nextInt(anz);
		Integer plz = (Integer) RandomUtils.randomOfObjectArray(orte.keySet().toArray());
		return new String[] {plz.toString(), orte.get(plz)};
	}
}
