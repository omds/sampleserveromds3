package at.vvo.omds3.exampleservice.config;

import java.util.List;

/**
 * Objekt welches die Einstellungen aus der Spring-Configuration zur Verfuegung stellt.
 * @author Jens Buehring
 *
 */
public class BackendConfigBeanImpl implements BackendConfigBean {

	String appid;
	int vunr;
	String dvrNr;
	List<User> listUsers;
	String omdsVersion;
	int minAnzVertraegeInit;
	int maxAnzVertraegeInit;
	int minAnzVertraegeErg;
	int maxAnzVertraegeErg;
	int minPersnnr;
	int maxPersnnr;
	int maxZusatz;
	int maxHausnr;
	int minAlter;
	int maxAlter;
	int minPolizzennr;
	int maxPolizzennr;
	int minPackageNr;
	int maxPackageNr;
	int intervalForModelupdateInSeconds;
	int hourOfDayForModelClean;
	int minuteOfHourForModelClean;
	int minutesOMDSDatensatzValid;
	ArtAuthentifizierung artAuthentifizierung;
	
	@Override
	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public int getVunr() {
		return vunr;
	}

	public void setVunr(int vunr) {
		this.vunr = vunr;
	}

	public ArtAuthentifizierung getArtAuthentifizierung() {
		return artAuthentifizierung;
	}

	public void setArtAuthentifizierung(ArtAuthentifizierung artAuthentifizierung) {
		this.artAuthentifizierung = artAuthentifizierung;
	}

	public List<User> getListUsers() {
		return listUsers;
	}

	public void setListUsers(List<User> listUsers) {
		this.listUsers = listUsers;
	}

	public String getDvrNr() {
		return dvrNr;
	}

	public void setDvrNr(String dvrNr) {
		this.dvrNr = dvrNr;
	}

	public String getOmdsVersion() {
		return omdsVersion;
	}

	public void setOmdsVersion(String omdsVersion) {
		this.omdsVersion = omdsVersion;
	}

	public int getMinAnzVertraegeInit() {
		return minAnzVertraegeInit;
	}

	public void setMinAnzVertraegeInit(int minAnzVertraegeInit) {
		this.minAnzVertraegeInit = minAnzVertraegeInit;
	}

	public int getMaxAnzVertraegeInit() {
		return maxAnzVertraegeInit;
	}

	public void setMaxAnzVertraegeInit(int maxAnzVertraegeInit) {
		this.maxAnzVertraegeInit = maxAnzVertraegeInit;
	}

	public int getMinPersnnr() {
		return minPersnnr;
	}

	public void setMinPersnnr(int minPersnnr) {
		this.minPersnnr = minPersnnr;
	}

	public int getMaxPersnnr() {
		return maxPersnnr;
	}

	public void setMaxPersnnr(int maxPersnnr) {
		this.maxPersnnr = maxPersnnr;
	}

	public int getMaxZusatz() {
		return maxZusatz;
	}

	public void setMaxZusatz(int maxZusatz) {
		this.maxZusatz = maxZusatz;
	}

	public int getMaxHausnr() {
		return maxHausnr;
	}

	public void setMaxHausnr(int maxHausnr) {
		this.maxHausnr = maxHausnr;
	}

	public int getMinAlter() {
		return minAlter;
	}

	public void setMinAlter(int minAlter) {
		this.minAlter = minAlter;
	}

	public int getMaxAlter() {
		return maxAlter;
	}

	public void setMaxAlter(int maxAlter) {
		this.maxAlter = maxAlter;
	}

	public int getMinPolizzennr() {
		return minPolizzennr;
	}

	public void setMinPolizzennr(int minPolizzennr) {
		this.minPolizzennr = minPolizzennr;
	}

	public int getMaxPolizzennr() {
		return maxPolizzennr;
	}

	public void setMaxPolizzennr(int maxPolizzennr) {
		this.maxPolizzennr = maxPolizzennr;
	}

	public int getMinAnzVertraegeErg() {
		return minAnzVertraegeErg;
	}

	public void setMinAnzVertraegeErg(int minAnzVertraegeErg) {
		this.minAnzVertraegeErg = minAnzVertraegeErg;
	}

	public int getMaxAnzVertraegeErg() {
		return maxAnzVertraegeErg;
	}

	public void setMaxAnzVertraegeErg(int maxAnzVertraegeErg) {
		this.maxAnzVertraegeErg = maxAnzVertraegeErg;
	}

	public int getMinPackageNr() {
		return minPackageNr;
	}

	public void setMinPackageNr(int minPackageNr) {
		this.minPackageNr = minPackageNr;
	}

	public int getMaxPackageNr() {
		return maxPackageNr;
	}

	public void setMaxPackageNr(int maxPackageNr) {
		this.maxPackageNr = maxPackageNr;
	}

	public int getIntervalForModelupdateInSeconds() {
		return intervalForModelupdateInSeconds;
	}

	public void setIntervalForModelupdateInSeconds(int intervalForModelupdateInSeconds) {
		this.intervalForModelupdateInSeconds = intervalForModelupdateInSeconds;
	}

	public int getHourOfDayForModelClean() {
		return hourOfDayForModelClean;
	}

	public void setHourOfDayForModelClean(int h) {
		this.hourOfDayForModelClean = h;
	}

	public int getMinutesOMDSDatensatzValid() {
		return minutesOMDSDatensatzValid;
	}

	public void setMinutesOMDSDatensatzValid(int m) {
		this.minutesOMDSDatensatzValid = m;
	}

	public int getMinuteOfHourForModelClean() {
		return minuteOfHourForModelClean;
	}

	public void setMinuteOfHourForModelClean(int m) {
		this.minuteOfHourForModelClean = m;
	}


	
}
