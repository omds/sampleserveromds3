package at.vvo.omds3.exampleservice.fake.omdsPackage;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import at.vvo.omds.types.omds2Types.v2_16.PERSONType;
import at.vvo.omds.types.omds2Types.v2_16.SPARTEType;
import at.vvo.omds.types.omds2Types.v2_16.VERTRAG;
import at.vvo.omds.types.omds2Types.v2_16.VERTRAGSPERSONType;
import at.vvo.omds.types.omds2Types.v2_16.VtgRolleCdType;
import at.vvo.omds.types.omds2Types.v2_16.WaehrungsCdType;
import at.vvo.omds3.exampleservice.config.AppContext;
import at.vvo.omds3.exampleservice.fake.StringUtils;

/**
 * Erzeugt eine Liste von neuen Vertagsobjekten.
 * 
 * @author Jens
 *
 */
public class CreatorVertrag {
	
	private static final String[] VTG_PROD_CD = new String[] {"99", "BS", "BT", "FL", "KE", "KF", "KG", "LE", "LG", "LS", "LW", "PR", "UF", "UG"};
	private static final Integer[] ZAHLRYTHMUS_CD = new Integer[] {0, 1, 2, 4, 5, 6, 9};
	private static final String[] ZAHLWEG_CD = new String[] {"00", "01", "02", "03", "04", "11", "12", "13", "14", "99"};
	private static final Integer[] VTG_STATUS_CD = new Integer[] {0, 1, 2, 3, 4, 9};
	private static final String[] AEND_GRUND_CD = new String[] {"999", "AEO", "AEP", "BMA", "GEM", "INA", "KON", "NEU", "PAR", "RET", "REV", "RUT", "RUV", "SPE", "STT", "STV", "TAA"};
	
	
	/**
	 * Constructor, der das Modell um eine vorgegebene Anzahl von Vertraegen erweitert.
	 *
	 * 
	 */
	public CreatorVertrag(
			int anzahl, 
			String maklerID, 
			String vermnr, 
			List<VERTRAG> listeVertraegeVermittler, 
			Map<String, PERSONType> personen, 
			Map<String, VERTRAG> vertraege
	) {
		listeVertraegeVermittler.addAll(erzeugeListeVertraege(anzahl, maklerID, vermnr, personen, vertraege));
	}

	public static List<VERTRAG> erzeugeListeVertraege(
			int anzahl, 
			String maklerID, 
			String vermnr, 
			Map<String, PERSONType> personen, 
			Map<String, VERTRAG> vertraege
	) {
		
		if (vermnr == null || vermnr.length() == 0)
			throw new IllegalArgumentException("Vermitternr darf nicht leer sein!");
		
		List<VERTRAG> liste = new ArrayList<>();
		for (int i = 0; i < anzahl; i++) {
			
			long jahrespraemieNetto = erzeugeRandomJahrespraemie(10000, 200000);
			int zahlrythmus = erzeugeRandomZahlrythmus(new Integer[] {1, 2, 4});
			
			VERTRAG v = erzeugeVertrag(vermnr, jahrespraemieNetto, zahlrythmus, personen, vertraege);
			liste.add(v);
		}
		return liste;
	}
	

	private static VERTRAG erzeugeVertrag(
			String vermnr, 
			long jahresPraemieNettoInCent, 
			int zahlrythmus, 
			Map<String, PERSONType> personen, 
			Map<String, VERTRAG> vertraege
	) {
		
		VERTRAG v = new VERTRAG();
		v.setPolizzennr(createPolizzennr(vertraege));
		v.setVermnr(vermnr);
		v.setVtgProdCd("KF"); // Kfz
		v.setVtgProdukt("Kraftfahrzeughaftpflicht"); // max 40
		v.setZahlRhythmCd(String.valueOf(zahlrythmus)); // jaehrlich
		v.setZahlWegCd("01"); // Zahlschein
		v.setVtgStatusCd("1"); // aktiv
		
		// Datuemer
		LocalDateTime now = LocalDateTime.now();
		v.setVtgBeg(StringUtils.dateToXmlGreogrianCalendar(now.minusYears(5)));
		v.setVtgEnd(StringUtils.dateToXmlGreogrianCalendar(now.plusYears(3)));
		LocalDateTime durchfuehrungsdatum = now.minusMonths(8);
		v.setDurchfDat(StringUtils.dateToXmlGreogrianCalendar(durchfuehrungsdatum));
		v.setGueltigBeg(StringUtils.dateToXmlGreogrianCalendar(StringUtils.bestimmeFolgendenMonatsersten(durchfuehrungsdatum)));
		
		v.setAendGrundCd(erzeugeAenderungsgrund());
		BigDecimal nettopraemieCentGemaessZahlweise = new BigDecimal(jahresPraemieNettoInCent).divide(new BigDecimal(zahlrythmus), 2, RoundingMode.HALF_UP);
		v.setPraemieNtoVtg(StringUtils.centToEuro(nettopraemieCentGemaessZahlweise));
		double steuersatz = 0.11;
		BigDecimal jahresPraemieBruttoInCent = nettopraemieCentGemaessZahlweise.multiply(new BigDecimal(1 + steuersatz));
		v.setPraemieBtoVtg(StringUtils.centToEuro(jahresPraemieBruttoInCent));
		v.setWaehrungsCd(WaehrungsCdType.EUR);
		v.setBIC("EASYATW1");
		v.setIBAN("AT96 1420 0200 1006 3345");
		
		v.getSPARTE().add(erzeugeSparte());
		
		// Ordner VN zu
		v.getVERTRAGSPERSON().add(ordeVersichertePersonZu(personen));
		return v;
	}
	
	private static String erzeugeAenderungsgrund() {
		return RandomUtils.randomOfStringArray(AEND_GRUND_CD);
	}
	
	private static int erzeugeRandomZahlrythmus(Integer[] integers) {
		return RandomUtils.randomOfIntegerArray(integers);
	}

	private static long erzeugeRandomJahrespraemie(int untereGrenzeCent, int obereGrenzeCent) {
		Random randomGenerator = new Random();
		int cent = randomGenerator.nextInt(obereGrenzeCent - untereGrenzeCent) + untereGrenzeCent;
		return cent;
	}
	
	private static VERTRAGSPERSONType ordeVersichertePersonZu(Map<String, PERSONType> personen) {
		return ordePersonZu(VtgRolleCdType.VN, personen);
	}
	
	private static VERTRAGSPERSONType ordePersonZu(VtgRolleCdType rolle, Map<String, PERSONType> personen) {
		boolean esGibtBereitsPersonen = personen.size() > 0;
		boolean neuePerson = true;
		if (esGibtBereitsPersonen) {
			neuePerson = RandomUtils.nextBoolean();
		}
		
		PERSONType person = null;
		if (neuePerson) {
			person = CreatorPerson.erzeugePerson(personen);
			personen.put(person.getPersonennr(), person);
		} else {
			// nehme eine bestehendePerson
			person = waehleBestehendePerson(personen);
			
		}
		
		// referenziere die Person als Vertragsperson, die Laufende Nummer wird hier allerdings noch nicht bestimmt
		return erzeugeVertragsrolle(0, person.getPersonennr(), rolle);
		
	}

	/**
	 * Bestimmt Zufaellig eine Person aus der Map der Personen
	 * @param personen eine Map mit Personennr und Personen
	 * @return die Person
	 */
	private static PERSONType waehleBestehendePerson(Map<String, PERSONType> personen) {
		String personennr = (String) RandomUtils.randomOfObjectArray(personen.keySet().toArray());
		return personen.get(personennr);
	}

	/**
	 * Erzeugt eine Vertragsrolle fuer eine bestimmte Person
	 * @param lfnr die Laufende Nummer
	 * @param personennr die Personennummer
	 * @param rolle die Art der Rolle
	 * @return die konkrete Rolle
	 */
	private static VERTRAGSPERSONType erzeugeVertragsrolle(int lfnr, String personennr, VtgRolleCdType rolle) {
		VERTRAGSPERSONType vertragsperson = new VERTRAGSPERSONType();
		vertragsperson.setLfnr(lfnr);
		vertragsperson.setPersonennr(personennr);
		vertragsperson.setVtgRolleCd(rolle);
		return vertragsperson;
	}

	/**
	 * Sparte ist ein Unterelement von Vertrag, z.B. Leitungswasser, Feuer, Haftpflicht o.ä.
	 * @return die Sparte
	 */
	private static SPARTEType erzeugeSparte() {
		SPARTEType sparte = new SPARTEType();
		
		return sparte;
	}

	private static String createPolizzennr(Map<String, VERTRAG> vertraege) {
		int min = AppContext.getAppConfig().getMinPolizzennr();
		int max = AppContext.getAppConfig().getMaxPolizzennr();
		return RandomUtils.createNr(min, max, vertraege.keySet());
	}
	
	
}
