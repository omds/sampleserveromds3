package at.vvo.omds3.exampleservice.config;

import java.util.List;

/**
 * Eine MaklerID dient der Identifizierung eines Maklers.
 * Einer MaklerID kann eine oder mehrere Vermittlernummern zugeordnet werden.
 * 
 * @author Jens
 *
 */
public class MaklerID {
	
	String maklerID;
	List<String> vermnr;
	
	public MaklerID() {
		super();
	}
	
	
	public String getMaklerID() {
		return maklerID;
	}
	public void setMaklerID(String maklerID) {
		this.maklerID = maklerID;
	}
	public List<String> getVermnr() {
		return vermnr;
	}
	public void setVermnr(List<String> vermnr) {
		this.vermnr = vermnr;
	}

	
}
