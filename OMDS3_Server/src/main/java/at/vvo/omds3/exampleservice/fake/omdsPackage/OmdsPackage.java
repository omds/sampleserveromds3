package at.vvo.omds3.exampleservice.fake.omdsPackage;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import at.vvo.omds.types.omds2Types.v2_16.*;

/**
 * Transportobjekt Server-Client fuer die Informationen zu einem OMDS-Package.
 * 
 * @author Jens Buehring
 *
 */
public class OmdsPackage implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String vuNr;
	String agentID;
	OmdsPackageIdDetails omdsPackageIdDetails;
	LocalDateTime timestamp;
	int packageSize;
	PaketUmfCdType packageExtent;
	List<PaketInhCdType> packageContentCodes;
	
	
	
	/**
	 * Default Constructor
	 */
	public OmdsPackage() {
		super();
	}

	
	/**
	 * Constructor
	 * @param vuNr
	 * @param agentID
	 * @param omdsPackageIdDetails
	 * @param timestamp
	 * @param packageSize
	 * @param packageExtent
	 * @param packageContentCodes
	 */
	public OmdsPackage(String vuNr, String agentID, OmdsPackageIdDetails omdsPackageIdDetails, LocalDateTime timestamp,
			int packageSize, PaketUmfCdType packageExtent, List<PaketInhCdType> packageContentCodes) {
		super();
		this.vuNr = vuNr;
		this.agentID = agentID;
		this.omdsPackageIdDetails = omdsPackageIdDetails;
		this.timestamp = timestamp;
		this.packageSize = packageSize;
		this.packageExtent = packageExtent;
		this.packageContentCodes = packageContentCodes;
	}



	public String getVuNr() {
		return vuNr;
	}
	public void setVuNr(String vuNr) {
		this.vuNr = vuNr;
	}
	public String getAgentID() {
		return agentID;
	}
	public void setAgentID(String agentID) {
		this.agentID = agentID;
	}
	public LocalDateTime getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
	public int getPackageSize() {
		return packageSize;
	}
	public void setPackageSize(int packageSize) {
		this.packageSize = packageSize;
	}
	public PaketUmfCdType getPackageExtent() {
		return packageExtent;
	}
	public void setPackageExtent(PaketUmfCdType packageExtent) {
		this.packageExtent = packageExtent;
	}
	public List<PaketInhCdType> getPackageContentCodes() {
		return packageContentCodes;
	}
	public void setPackageContentCodes(List<PaketInhCdType> l) {
		this.packageContentCodes = l;
	}
	public OmdsPackageIdDetails getOmdsPackageIdDetails() {
		return omdsPackageIdDetails;
	}
	public void setOmdsPackageIdDetails(OmdsPackageIdDetails omdsPackageIdDetails) {
		this.omdsPackageIdDetails = omdsPackageIdDetails;
	}
	
	
}
