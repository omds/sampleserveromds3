package at.vvo.omds3.exampleservice.fake.polDocuments;

import java.io.Serializable;

public class MCArcImageResponse implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected MCArcContent arcContent;
//    protected ServiceFault serviceFault;

	public MCArcContent getArcContent() {
		return arcContent;
	}

	public void setArcContent(MCArcContent arcContent) {
		this.arcContent = arcContent;
	}
    
    

}
