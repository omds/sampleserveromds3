package at.vvo.omds3.exampleservice.fake.polDocuments;

import java.io.Serializable;


public class MCArcContent implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected MCArcImageInfo arcImageInfo;
    protected byte[] arcImage;
    
	public MCArcImageInfo getArcImageInfo() {
		return arcImageInfo;
	}
	public void setArcImageInfo(MCArcImageInfo arcImageInfo) {
		this.arcImageInfo = arcImageInfo;
	}
	public byte[] getArcImage() {
		return arcImage;
	}
	public void setArcImage(byte[] arcImage) {
		this.arcImage = arcImage;
	}
    
    

}
