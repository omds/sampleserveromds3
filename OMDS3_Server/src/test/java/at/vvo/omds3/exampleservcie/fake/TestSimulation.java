package at.vvo.omds3.exampleservcie.fake;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import at.vvo.omds.types.omds2Types.v2_16.PERSONType;
import at.vvo.omds.types.omds2Types.v2_16.VERTRAG;
import at.vvo.omds3.exampleservice.fake.StringUtils;
import at.vvo.omds3.exampleservice.fake.omdsPackage.CreatorPerson;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class TestSimulation {

	@Test
	public void test1() {
		BigDecimal bd = StringUtils.centToEuro(new BigDecimal(34545242));
		assertEquals("345452.42", bd.toString());
	}

	@Test
	public void test2() {
		
		Map<String, PERSONType> personen = new HashMap<>();
		PERSONType p = CreatorPerson.erzeugePerson(personen);
		personen.put(p.getPersonennr(), p);
		assertEquals(1, personen.size());
	}

	@Test
	public void test3() {
		Map<String, PERSONType> personen = new HashMap<>();
		Map<String, VERTRAG> vertraege = new HashMap<>();
//		List<VERTRAGType> liste = CreatorVertrag.erzeugeListeVertraege(2, "4523432", "7456533", personen, vertraege);
//		assertEquals(2, liste.size());
	}
}
